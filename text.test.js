// 🔤🟠 Kijch ∷ text.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import {
  assert,
  assertEquals,
  assertSpyCalls,
  assertStrictEquals,
  assertThrows,
  describe,
  it,
  spy,
} from "./dev-deps.js";
import { Cluster, Span, Text } from "./text.js";
import requiredCharacters from "./requiredCharacters.js";

describe("Span", () => {
  it("[[Construct]] throws an error", () => {
    assertThrows(() => new Span());
  });

  describe("::characters", () => {
    it("[[Call]] yields items", () => {
      const test = ["one", true, 3];
      assertEquals(
        [...Span.prototype.characters.call(test)],
        test,
      );
    });
  });

  describe("::codepoints", () => {
    it("[[Call]] yields codepoints", () => {
      assertEquals(
        [...Span.prototype.codepoints.call([{
          valueOf() {
            return 13;
          },
        }, 12])],
        [13, 12],
      );
    });

    it("[[Call]] coerces strange values into valid codepoints", () => {
      assertEquals(
        [...Span.prototype.codepoints.call(["one", true, 3])],
        [requiredCharacters.REPLACEMENT.codepoint, 1, 3],
      );
    });
  });

  describe("::toString", () => {
    it("[[Call]] catenates the string values of its items", () => {
      assertStrictEquals(
        Span.prototype.toString.call([{
          toString() {
            return "AC";
          },
        }, "AB"]),
        "ACAB",
      );
    });
  });

  describe("::[Symbol.iterator]", () => {
    it("[[Call]] throws on yield if this is not a constructed Span", () => {
      assertThrows(() =>
        Span.prototype[Symbol.iterator].call([]).next()
      );
    });

    it("[[Call]] yields codepoints for a constructed Span", () => {
      assertEquals(
        [...Span.prototype[Symbol.iterator].call(
          new Text(
            requiredCharacters,
            [requiredCharacters.FILLER, requiredCharacters.TIMES],
          ),
        )],
        [requiredCharacters.FILLER, requiredCharacters.TIMES],
      );
    });
  });
});

describe("Cluster", () => {
  it("[[Construct]] throws an error", () => {
    assertThrows(() => new Cluster());
  });

  describe("::base", () => {
    it("[[Get]] returns the first character", () => {
      assertStrictEquals(
        Reflect.get(
          Cluster.prototype,
          "base",
          [
            requiredCharacters.REPLACEMENT,
            requiredCharacters["MULTIGRAPH SEPARATOR"],
          ],
        ),
        requiredCharacters.REPLACEMENT,
      );
    });

    it("[[Get]] returns undefined if there are no characters", () => {
      assertStrictEquals(
        Reflect.get(Cluster.prototype, "base", []),
        undefined,
      );
    });

    it("[[Set]] fails to set the base", () => {
      assertStrictEquals(
        Reflect.set(
          Cluster.prototype,
          "base",
          requiredCharacters.OBJECT,
          [requiredCharacters.REPLACEMENT],
        ),
        false,
      );
    });
  });

  describe("::getTag", () => {
    it("[[Call]] returns the value of the corresponding tag", () => {
      const cluster = Object.assign(
        [requiredCharacters.REPLACEMENT],
        {
          *tags() {
            yield [requiredCharacters.REFERENCE, "example:reference"];
          },
        },
      );
      assertStrictEquals(
        Cluster.prototype.getTag.call(
          cluster,
          requiredCharacters.REFERENCE,
        ),
        "example:reference",
      );
    });

    it("[[Call]] returns undefined if there is no corresponding tag", () => {
      const cluster = Object.assign(
        [requiredCharacters.REPLACEMENT],
        {
          *tags() {
            yield [requiredCharacters.REFERENCE, "example:reference"];
          },
        },
      );
      assertStrictEquals(
        Cluster.prototype.getTag.call(
          cluster,
          requiredCharacters.IDENTIFIER,
        ),
        undefined,
      );
    });
  });

  describe("::length", () => {
    it("[[Get]] returns the length", () => {
      assertStrictEquals(
        Reflect.get(
          Cluster.prototype,
          "length",
          [requiredCharacters.REPLACEMENT],
        ),
        1,
      );
    });

    it("[[Set]] fails to set the length", () => {
      assertStrictEquals(
        Reflect.set(
          Cluster.prototype,
          "length",
          0,
          [requiredCharacters.REPLACEMENT],
        ),
        false,
      );
    });
  });

  describe("::tags", () => {
    it("[[Call]] throws on yield if this is not a constructed Cluster", () => {
      assertThrows(() => Cluster.prototype.tags.call([]).next());
    });

    it("[[Call]] yields character~value pairs for a constructed Cluster", () => {
      const cluster = new Text(
        requiredCharacters,
        [
          requiredCharacters.REFERENCE,
          requiredCharacters["TAG-0065"],
          requiredCharacters["TAG-0078"],
          requiredCharacters["TAG-0061"],
          requiredCharacters["TAG-006D"],
          requiredCharacters["TAG-0070"],
          requiredCharacters["TAG-006C"],
          requiredCharacters["TAG-0065"],
          requiredCharacters["TAG-003A"],
          requiredCharacters.REPLACEMENT,
        ],
      ).clusters().next().value;
      assertEquals(
        [...Cluster.prototype.tags.call(cluster)],
        [[requiredCharacters.REFERENCE, "example:"]],
      );
    });
  });

  // TK: `::toImageData`

  describe("~length", () => {
    it("[[Get]] returns the length", () => {
      const cluster = new Text(
        requiredCharacters,
        [requiredCharacters.REPLACEMENT],
      ).clusters().next().value;
      assertStrictEquals(cluster.length, 1);
    });

    it("[[Set]] fails to set the length", () => {
      const cluster = new Text(
        requiredCharacters,
        [requiredCharacters.REPLACEMENT],
      ).clusters().next().value;
      assertStrictEquals(Reflect.set(cluster, "length", 0), false);
    });
  });
});

describe("Text", () => {
  it("[[Construct]] creates a new Text containing the provided codepoints", () => {
    const text = new Text(
      requiredCharacters,
      [requiredCharacters.FILLER, requiredCharacters.TIMES],
    );
    assert(text instanceof Text);
    assert(text instanceof Span);
    assertEquals(
      [...text],
      [requiredCharacters.FILLER, requiredCharacters.TIMES],
    );
  });

  it("[[Construct]] replaces missing characters with REPLACEMENT", () => {
    assertEquals(
      [...new Text(requiredCharacters, [0x80])],
      [requiredCharacters.REPLACEMENT],
    );
  });

  it("[[Construct]] replaces control characters with REPLACEMENT", () => {
    assertEquals(
      [...new Text(requiredCharacters, [0x01])],
      [requiredCharacters.REPLACEMENT],
    );
  });

  it("[[Construct]] replaces messaging characters with REPLACEMENT", () => {
    assertEquals(
      [...new Text(requiredCharacters, [0x07])],
      [requiredCharacters.REPLACEMENT],
    );
  });

  it("[[Construct]] replaces invalid characters with REPLACEMENT", () => {
    assertEquals(
      [...new Text(requiredCharacters, [0x0F])],
      [requiredCharacters.REPLACEMENT],
    );
  });

  it("[[Construct]] drops NULL characters", () => {
    assertEquals(
      [
        ...new Text(
          requiredCharacters,
          [
            0,
            requiredCharacters.FILLER,
            0,
            requiredCharacters.TIMES,
            0,
            0,
            requiredCharacters.NOTHING,
            0,
          ],
        ),
      ],
      [
        requiredCharacters.FILLER,
        requiredCharacters.TIMES,
        requiredCharacters.REPLACEMENT,
      ],
    );
  });

  it("[[Construct]] replaces valid embeds with no media type with OBJECT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters.DATA,
          requiredCharacters["STRING BEGIN"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          0xACAB,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.OBJECT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] replaces valid embeds with an HTTP media type with OBJECT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters.DATA,
          0x21,
          requiredCharacters["STRING BEGIN"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          0xACAB,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.OBJECT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] replaces valid embeds with a URI media type with OBJECT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters.DATA,
          requiredCharacters.LEAVE,
          requiredCharacters.RETURN,
          requiredCharacters["STRING BEGIN"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          0xACAB,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.OBJECT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] replaces valid shifts with no URI with REPLACEMENT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters.SHIFT,
          0xACAB,
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.REPLACEMENT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] replaces valid shifts with an URI with REPLACEMENT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters.SHIFT,
          requiredCharacters.LEAVE,
          requiredCharacters.RETURN,
          0xACAB,
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.REPLACEMENT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] replaces valid shift strings with no URI with OBJECT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters["SHIFT STRING"],
          requiredCharacters["STRING BEGIN"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          0xACAB,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.OBJECT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] replaces valid shift strings with a URI with OBJECT", () => {
    assertEquals(
      [
        ...new Text(requiredCharacters, [
          requiredCharacters["SHIFT STRING"],
          requiredCharacters.LEAVE,
          requiredCharacters.RETURN,
          requiredCharacters["STRING BEGIN"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          0xACAB,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.DONT,
          requiredCharacters.DONT,
          requiredCharacters["STRING FINISH"],
          requiredCharacters.FILLER,
        ]),
      ],
      [requiredCharacters.OBJECT, requiredCharacters.FILLER],
    );
  });

  it("[[Construct]] is lazy", () => {
    const spyFn = spy(() => requiredCharacters.FILLER);
    const text = new Text(
      requiredCharacters,
      function* () {
        while (true) {
          yield spyFn();
        }
      }(),
    );
    const iterator = text[Symbol.iterator]();
    assertSpyCalls(spyFn, 0);
    assertStrictEquals(
      iterator.next().value,
      requiredCharacters.FILLER,
    );
    assertSpyCalls(spyFn, 1);
  });

  it("[[Construct]] caches yields", () => {
    const spyFn = spy(() => requiredCharacters.FILLER);
    const text = new Text(
      requiredCharacters,
      function* () {
        while (true) {
          yield spyFn();
        }
      }(),
    );
    const iterator = text[Symbol.iterator]();
    assertSpyCalls(spyFn, 0);
    assertStrictEquals(
      iterator.next().value,
      requiredCharacters.FILLER,
    );
    assertSpyCalls(spyFn, 1);
    const iterator2 = text[Symbol.iterator]();
    assertStrictEquals(
      iterator2.next().value,
      requiredCharacters.FILLER,
    );
    assertSpyCalls(spyFn, 1);
    assertStrictEquals(
      iterator2.next().value,
      requiredCharacters.FILLER,
    );
    assertSpyCalls(spyFn, 2);
    assertStrictEquals(
      iterator.next().value,
      requiredCharacters.FILLER,
    );
    assertSpyCalls(spyFn, 2);
  });

  // TK: `.fromDomNode`

  describe(".fromUnicode", () => {
    it("[[Call]] maps from Unicode strings into the charset", () => {
      assertEquals(
        [...Text.fromUnicode(
          requiredCharacters,
          `${requiredCharacters.FILLER}${requiredCharacters.TIMES}`,
        )],
        [requiredCharacters.FILLER, requiredCharacters.TIMES],
      );
    });

    it("[[Call]] maps from Unicode codepoint iterables into the charset", () => {
      assertEquals(
        [...Text.fromUnicode(
          requiredCharacters,
          [
            requiredCharacters.FILLER,
            requiredCharacters.TIMES,
          ].flatMap(($) => $.unicode),
        )],
        [requiredCharacters.FILLER, requiredCharacters.TIMES],
      );
    });

    it("[[Call]] sanitizes text", () => {
      assertEquals(
        [...Text.fromUnicode(
          requiredCharacters,
          `${requiredCharacters.NOTHING}${requiredCharacters.FILLER}x`,
        )],
        [
          requiredCharacters.REPLACEMENT,
          requiredCharacters.FILLER,
          requiredCharacters.REPLACEMENT,
        ],
      );
    });

    it("[[Call]] maps multi‐character sequences", () => {
      const charA = {
        basicType: requiredCharacters.REPLACEMENT.basicType,
        codepoint: 0x90A1,
        unicode: [0x0061],
        valueOf() {
          return 0x90A1;
        },
      };
      const charB = {
        basicType: requiredCharacters.REPLACEMENT.basicType,
        codepoint: 0x90A2,
        unicode: [0x0062],
        valueOf() {
          return 0x90A2;
        },
      };
      const charR = {
        basicType: requiredCharacters.REPLACEMENT.basicType,
        codepoint: 0x90B2,
        unicode: [0x0072],
        valueOf() {
          return 0x90B2;
        },
      };
      const charZ = {
        basicType: requiredCharacters.REPLACEMENT.basicType,
        codepoint: 0x90BA,
        unicode: [0x007A],
        valueOf() {
          return 0x90BA;
        },
      };
      const charBAR = {
        codepoint: 0x90A0,
        basicType: requiredCharacters.REPLACEMENT.basicType,
        unicode: [0x0062, 0x0061, 0x0072],
        valueOf() {
          return 0x90A0;
        },
      };
      assertEquals(
        [...Text.fromUnicode(
          [
            ...requiredCharacters,
            charA,
            charB,
            charR,
            charZ,
            charBAR,
          ],
          `barbaz`,
        )],
        [charBAR, charB, charA, charZ],
      );
    });
  });

  describe("::clusters", () => {
    it("[[Call]] yields the clusters", () => {
      assertEquals(
        [...Text.prototype.clusters.call([
          requiredCharacters.REPLACEMENT,
          requiredCharacters["MULTIGRAPH SEPARATOR"],
          requiredCharacters.REPLACEMENT,
        ])].map(($) => [...$]),
        [[
          requiredCharacters.REPLACEMENT,
          requiredCharacters["MULTIGRAPH SEPARATOR"],
        ], [requiredCharacters.REPLACEMENT]],
      );
    });

    it("[[Call]] keeps line breaks separate", () => {
      assertEquals(
        [...Text.prototype.clusters.call([
          requiredCharacters["LINE SEPARATOR"],
          requiredCharacters["MULTIGRAPH SEPARATOR"],
        ])].map(($) => [...$]),
        [
          [requiredCharacters["LINE SEPARATOR"]],
          [requiredCharacters["MULTIGRAPH SEPARATOR"]],
        ],
      );
    });

    it("[[Call]] ignores ignorables", () => {
      assertEquals(
        [...Text.prototype.clusters.call([
          requiredCharacters.REPLACEMENT,
          requiredCharacters.HYPHENATION,
          requiredCharacters["MULTIGRAPH SEPARATOR"],
        ])].map(($) => [...$]),
        [[
          requiredCharacters.REPLACEMENT,
          requiredCharacters["MULTIGRAPH SEPARATOR"],
        ]],
      );
    });
  });

  // TK: `::lines`
});
