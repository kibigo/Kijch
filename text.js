// 🔤🟠 Kijch ∷ text.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { Character } from "./charset.js";
import { kixtNamespace, xhtmlNamespace } from "./names.js";
import requiredCharacters from "./requiredCharacters.js";

const {
  /**
   * The internal, constructable implementation of the `Span` class.
   */
  Span,

  /**
   * A class providing common functionality for iterables of
   * characters.
   *
   * ☡ This class cannot be constructed.
   */
  spanConstructor,
} = (() => {
  class Span extends function ($) {
    return $;
  } {
    /**
     * A function returning an iterable of characters in this `Span`
     */
    #getCharacters;

    /**
     * Constructs a new `Span` from the provided generator of
     * characters.
     *
     * If a second argument is provided, the `Span` will be directly
     * implemented onto it instead of creating a new object. This is
     * useful for subclassing.
     *
     * ※ This constructor is not exposed.
     */
    constructor(
      characterGetter,
      thisObject = Object.create(spanPrototype),
    ) {
      super(thisObject);
      this.#getCharacters = characterGetter;
    }

    /**
     * Returns an iterator over the characters in this iterable of
     * characters.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    *characters() {
      yield* this;
    }

    /**
     * Returns an iterator over the code·points in this iterable of
     * characters.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    *codepoints() {
      for (const character of this) {
        // Iterate over each character of this span and yield its
        // code·point.
        const result = +character;
        if (result !== Math.min(Math.max(result | 0, 0), 0xFFFF)) {
          // The code·point is out‐of‐range. Rather than throw an
          // error, just yield the replacement code·point.
          //
          // ※ This behaviour assumes a text‐compatible charset.
          yield +requiredCharacters.REPLACEMENT;
        } else {
          yield result;
        }
      }
    }

    /**
     * Returns the Unicode string value of this iteratable of
     * characters.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    toString() {
      return Array.from(this).map(($) => $.toString()).join("");
    }

    /**
     * Returns an iterable iterator over the characters in the span.
     *
     * ☡ This generator is **not** generic and will throw on the first
     * call to `.next()` if `this` was not constructed using the `Span`
     * constructor.
     */
    *[Symbol.iterator]() {
      yield* this.#getCharacters();
    }
  }
  const spanConstructor = class {
    /**
     * Constructs a new `Span`.
     *
     * ☡ This constructor always throws.
     */
    constructor() {
      throw new TypeError("Invalid constructor.");
    }
  };
  Object.defineProperties(spanConstructor, {
    name: { value: "Span" },
  });
  const spanPrototype = Object.defineProperties(
    spanConstructor.prototype,
    Object.assign(
      Object.getOwnPropertyDescriptors(Span.prototype),
      { constructor: { value: spanConstructor } },
    ),
  );
  return { Span, spanConstructor };
})();
export { spanConstructor as Span };

const {
  /**
   * The internal, constructable implementation of the `Cluster` class.
   */
  Cluster,

  /**
   * A cluster consisting of a base character and zero or more
   * modifiers (combining characters, ⁊·c).
   *
   * The properties of `Cluster`s are conceptually those of its base
   * characters.
   *
   * ☡ This class cannot be constructed.
   */
  clusterConstructor,
} = (() => {
  class Cluster extends Span {
    /** An `Array` storing all the characters in this cluster. */
    #characters;

    /**
     * A `Map` mapping identifier code·points to tags for this cluster.
     *
     * Tags themselves are represented as identifier‐value pairs.
     */
    #tags;

    /**
     * Constructs a new `Cluster` from the provided characters and
     * having the provided tags.
     *
     * ※ This constructor is not exposed.
     */
    constructor(characters, tagPairs) {
      const charArray = Array.from(characters);
      super(
        Array.prototype[Symbol.iterator].bind(charArray),
        Object.create(clusterPrototype),
      );
      this.#characters = charArray;
      this.#tags = new Map(
        function* () {
          for (const tagPair of tagPairs) {
            // Iterate over each tag pair and yield the ones whose
            // code·points are in‐range.
            const [codepoint] = tagPair;
            if (codepoint >= 0 && codepoint <= 0xFFFF) {
              // The code·point is in‐range.
              yield tagPair;
            } else {
              // The code·point is not in‐range; ignore it.
              /* do nothing */
            }
          }
        }(),
      );
      Object.defineProperty(this, "length", {
        configurable: false,
        enumerable: false,
        value: charArray.length,
        writable: false,
      });
    }

    /**
     * Returns the first character in this cluster.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    get base() {
      if (#characters in this) {
        return this.#characters[0];
      } else {
        for (const character of this) {
          // Iterate over this, but immediately return the first
          // character.
          return character;
        }
        return undefined;
      }
    }

    /**
     * Returns the value of the tag with the provided identifier,
     * or undefined if no such tag is defined on the base character of
     * this cluster.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters with a `tags` method.
     */
    getTag(identifier) {
      const codepoint = identifier == null ? null : +identifier;
      if (#tags in this) {
        // This is a `Cluster` proper; access the value from the tags
        // `Map`.
        return this.#tags.get(codepoint)?.[1];
      } else {
        // This is not a proper `Cluster`; iterate over the `.tags()`
        // and search for a match.
        for (const [idChar, value] of this.tags()) {
          // Iterate over each tag in tags and see if it matches.
          const idCodepoint = idChar == null ? null : +idChar;
          if (codepoint === idCodepoint) {
            // The code·point of the current tag’s identifier matches
            // the provided one.
            return value;
          } else {
            // The code·point of the current tag’s identifier does not
            // match the provided one.
            continue;
          }
        }
        return undefined;
      }
    }

    /**
     * Returns the number of characters in this `Cluster`.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     *
     * ※ For objects created by the `Cluster` constructor, this
     * property is shadowed by an own property with the same value.
     */
    get length() {
      if (#characters in this) {
        return this.#characters.length;
      } else {
        return [...this].length;
      }
    }

    /**
     * Yields arrays of tag identification characters and corresponding
     * (ascii) tag values for the base character of this `Cluster`.
     *
     * ☡ This function is **not** generic.
     */
    *tags() {
      yield* this.#tags.values();
    }

    /**
     * Returns an `ImageData` of the provided `size` representing this
     * `Cluster`.
     *
     * The second argument may specify the `color` of the resulting
     * glyph and whether a `fullwidth` representation should be
     * preferred.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    toImageData(fontSize, options = {}) {
      const size = (() => {
        const result = +fontSize;
        return Number.isNaN(result) ? 0 : Math.min(
          Math.max(result, 0),
          0x3FFF, // (0x3FFF * 4) ** 2 is a little less than 2 ** 32.
        );
      })();
      const color = (
        options.color ?? 0x000000FF
      ) >>> 0; // `>>>` is needed here to fit 0xFFFFFFFF in the result
      const fullwidth = !!options.fullwidth;
      const base = Reflect.get(Cluster.prototype, "base", this);
      const codepoint = Character.prototype.valueOf.call(base);
      const ensWide = base.basicType == `${kixtNamespace}FORMAT`
        ? 1 + !!(requiredCharacters[codepoint]?.fullwidth ?? fullwidth)
        : 1 + !!(base.fullwidth ?? fullwidth);
      const glyphWidth = Math.floor(size / (3 - ensWide));
      const bitArray = new Array(size).fill().map(
        () => new Array(glyphWidth).fill(0),
      );
      for (const character of this) {
        // Iterate over each character in this `Cluster` and draw ink
        // for the best appropriate glyph.
        const { glyphs } = character;
        const [width, height] = Object.keys(glyphs).reduce(
          ([bestWidth, bestHeight], key) => {
            const [width, height] = key.split("×").map(
              ($) => parseInt($),
            );
            if (!(`${width}×${height}` in glyphs)) {
              // The width and height in this key were not canonical
              // integers.
              return [bestWidth, bestHeight];
            } else if (bestHeight == height) {
              // The current height matches the current best height;
              // pick the width which corresponds to the `fullwidth`
              // option.
              return fullwidth && width == height ||
                  !fullwidth && bestWidth == bestHeight
                ? [width, height]
                : [bestWidth, bestHeight];
            } else if (bestHeight <= size) {
              // The current best height is less than the provided
              // size; the current height must be larger (but still
              // less than the provided size) to be better.
              return height > bestHeight && height <= size
                ? [width, height]
                : [bestWidth, bestHeight];
            } else {
              // The current best height is greater than the provided
              // size; any smaller height is better.
              return height < bestHeight
                ? [width, height]
                : [bestWidth, bestHeight];
            }
          },
          [Infinity, Infinity],
        );
        if (!isFinite(height)) {
          // No size was found (the current character has no glyphs).
          continue;
        } else {
          // A best size was found; add its bits to the bit array.
          const bits = Array.from(glyphs[`${width}×${height}`])
            .flatMap(
              (hex) => {
                const n = parseInt(hex, 16);
                return [n & 8, n & 4, n & 2, n & 1];
              },
            );
          for (const [rowIndex, row] of bitArray.entries()) {
            // Iterate over the rows in the bit array and set the
            // values appropriately.
            const { length } = row;
            const colOffset = Math.max((row.length - width) / 2, 0) |
              0;
            for (
              let colIndex = colOffset;
              colIndex < length && colIndex - colOffset < width;
              ++colIndex
            ) {
              // Iterate over the columns in the row (centring the
              // image if it is narrower than the available space) and
              // OR their bits with those in the glyph.
              row[colIndex] |=
                bits[rowIndex * width + colIndex - colOffset];
            }
          }
        }
      }
      return new ImageData(
        Uint8ClampedArray.from(
          function* () {
            for (const row of bitArray) {
              // Iterate over each row and yield the colours in the
              // columns.
              for (const col of row) {
                // Iterate over the columns and yield their colours.
                yield* !col ? [0x00, 0x00, 0x00, 0x00] : [
                  (color & 0xFF000000) >>> 24,
                  (color & 0x00FF0000) >>> 16,
                  (color & 0x0000FF00) >>> 8,
                  color & 0x000000FF,
                ];
              }
            }
          }(),
        ),
        glyphWidth,
        size,
      );
    }
  }
  const clusterConstructor = class extends spanConstructor {};
  Object.defineProperties(clusterConstructor, {
    name: { value: "Cluster" },
  });
  const clusterPrototype = Object.defineProperties(
    clusterConstructor.prototype,
    Object.assign(
      Object.getOwnPropertyDescriptors(Cluster.prototype),
      { constructor: { value: clusterConstructor } },
    ),
  );
  return { Cluster, clusterConstructor };
})();
export { clusterConstructor as Cluster };

const {
  /**
   * An iterable span of characters which may be laid out and rendered
   * to a canvas.
   *
   * ☡ This class cannot be constructed.
   */
  textConstructor,
} = (() => {
  /**
   * An iterator which wraps another iterator and allows one to peek
   * ahead at future values.
   */
  class PeekableIterator {
    /** The current value of the iterator. */
    #currentValue = undefined;

    /**
     * Whether the internal iterator is exhausted.
     *
     * ※ This `PeekableIterator` may still have values if `~#toYield`
     * is nonempty.
     */
    #done = false;

    /** The iterator that this `PeekableIterator` wraps. */
    #iterator;

    /** A mapping function to apply to iterator values. */
    #mapFn;

    /** An array of items to yield in future calls to `::next` */
    #toYield = [];

    /**
     * Constructs a new `PeekableIterator` from the provided iterable
     * object.
     *
     * If a second argument is provided, it is used as a mapping
     * function to be applied to values yielded from the iterable’s
     * iterator.
     */
    constructor(iterable, mapFn = undefined) {
      this.#iterator = iterable[Symbol.iterator]();
      this.#mapFn = mapFn;
    }

    /**
     * Returns the current value (the value given in the previous call
     * to `::next`).
     */
    get currentValue() {
      return this.#currentValue;
    }

    /**
     * Returns whether all values are known to have been exhausted.
     *
     * ☡ This will return `false` until `::next` has been called after
     * the final value.
     */
    get done() {
      return this.#toYield.length === 0 && this.#done;
    }

    /**
     * Advances the iterator as though `::next` were called the
     * specified number of times.
     */
    drop(distance = 1) {
      const iterator = this.#iterator;
      const toYield = this.#toYield;
      const mapFn = this.#mapFn;
      const n = +distance;
      if (!Number.isFinite(n) || n < -1 || n > -1 >>> 0) {
        // The drop distance is out of range.
        /* do nothing */
      } else if (n >= 1) {
        // The drop distance is at least one.
        let remaining = (n >>> 0) - toYield.length;
        let done = this.#done;
        let value = toYield.splice(0, n).pop();
        while (!done && remaining-- > 0) {
          // Iterate while the iterator is not exhausted and more drops
          // are necessary, remembering the latest value.
          ({ value, done } = iterator.next());
          if (mapFn != null) {
            // A mapping function is defined.
            value = mapFn(value);
          } else {
            // No mapping function is defined.
            /* do nothing */
          }
        }
        this.#currentValue = done ? undefined : value;
        this.#done = done;
      } else {
        // The distance is not at least one.
        /* do nothing */
      }
    }

    /**
     * Returns an iterator result object giving the next value and
     * whether or not iteration has been exhausted.
     */
    next() {
      const toYield = this.#toYield;
      if (toYield.length > 0) {
        // There are remembered items from a previous `.peek()`.
        const value = this.#currentValue = toYield.shift();
        return { value, done: false };
      } else if (this.#done) {
        // There are no remembered items from a previous `.peek()` and
        // the iterator has been exhausted.
        return {
          value: undefined,
          done: true,
        };
      } else {
        // There are no remembered items from a previous `.peek()` and
        // the iterator may still have items.
        const mapFn = this.#mapFn;
        const { value, done } = this.#iterator.next();
        if (done) {
          // The iterator did not, in fact, have items, and is now
          // exhausted.
          this.#currentValue = undefined;
          this.#done = true;
          return { value: undefined, done };
        } else {
          // The iterator yielded another item.
          const mapped = mapFn == null ? value : mapFn(value);
          this.#currentValue = mapped;
          return { value: mapped, done: false };
        }
      }
    }

    /**
     * Returns an upcoming next value.
     *
     * The provided argument specifies how many `::next` calls lie
     * between the current value and the requested one.
     */
    peek(distance = 1) {
      const toYield = this.#toYield;
      const { length: toYieldLength } = toYield;
      const n = +distance;
      if (!Number.isFinite(n) || n < -1 || n > -1 >>> 0) {
        // The peek distance is out of range.
        return undefined;
      } else if (n < 1) {
        // The peek distance is less than one; return the current
        // value.
        return this.#currentValue;
      } else {
        // The peek distance is greater than one.
        const desiredIndex = (n >>> 0) - 1;
        if (desiredIndex < toYieldLength) {
          // The item at the peek distance has already been recorded;
          // return it.
          return toYield[desiredIndex];
        } else if (this.#done) {
          // There is no item at the peek distance but the iterator has
          // been exhausted.
          return undefined;
        } else {
          // There is an item at the peek distance but the iterator has
          // not yet been exhausted.
          const iterator = this.#iterator;
          const mapFn = this.#mapFn;
          let nextIndex = toYieldLength;
          while (true) {
            const { value, done } = iterator.next();
            if (done) {
              // The iterator has now been exhausted; there was no
              // item.
              this.#done = true;
              return undefined;
            } else {
              // The iterator yielded another item; remember it for
              // later.
              const mapped = mapFn == null ? value : mapFn(value);
              toYield.push(mapped);
              if (nextIndex++ < desiredIndex) {
                // The number of remembered items still is less than
                // the peek distance.
                /* do nothing */
              } else {
                // The number of remembered items now matches the peek
                // distance; return the last item.
                return mapped;
              }
            }
          }
        }
      }
    }

    /**
     * Returns this `PeekableIterator`, allowing it to be used where
     * iterables are expected.
     */
    [Symbol.iterator]() {
      return this;
    }
  }

  /**
   * Yields characters in the provided charset which correspond to
   * those in the provided iterable of code·points.
   */
  function charactersFromCodepoints(charset, codepoints) {
    const definitions = textCompatibleDefinitions(charset);
    return sanitized(definitions, codepoints);
  }

  /**
   * Yields characters in the provided charset which correspond to
   * those in the provided iterable of code·points.
   *
   * ☡ Because this requires not only Dom tree processing but also
   * converting from Unicode, this is a pretty expensive generator.
   */
  function charactersFromDomNode(charset, node) {
    const definitions = textCompatibleDefinitions(charset);
    return sanitized(
      definitions,
      codepointsFromDomNode(definitions, node),
    );
  }

  /**
   * Yields characters in the provided charset which correspond to
   * those in the provided Unicode string or iterable of Unicode
   * code·points.
   *
   * If a third argument is provided, the resulting characters will be
   * remapped onto those characters which have compatibility mappings
   * of the provided mode, should they exist.
   *
   * If no suitable mapping can be found, unicode characters will map
   * to REPLACEMENT.
   *
   * ※ If it is possible to map a sequence of Unicode characters to a
   * single character in the provided charset, this function will
   * attempt to do so. However, where multiple valid mappings exist, it
   * cannot guarantee producing the smallest.
   *
   * ☡ Converting between character sets is a necessarily complex
   * operation; try not to call this function multiple times from
   * performance‐sensitive code.
   */
  function charactersFromString(charset, stringOrIterable) {
    const unicodeCodepoints = typeof stringOrIterable == "string"
      ? function* () {
        for (const char of stringOrIterable) {
          yield char.codePointAt(0);
        }
      }()
      : stringOrIterable;
    const definitions = textCompatibleDefinitions(charset);
    return sanitized(
      definitions,
      codepointsFromUnicode(definitions, unicodeCodepoints),
    );
  }

  /**
   * Mutates the provided array of clusters by popping any spaces off
   * of the end of it and removing any WORD SEPARATORs.
   */
  const cleanLine = (line) => {
    let lineLength;
    while ((lineLength = line.length) > 0) {
      // Iterate (backwards) over each cluster in the line and pop for
      // as long as they are JUSTIFIABLE QUAD.
      const lastCluster = line[lineLength - 1];
      if (
        lastCluster.length == 1 &&
        +lastCluster.base == requiredCharacters["JUSTIFIABLE QUAD"]
      ) {
        // The final cluster is a single JUSTIFIABLE QUAD character.
        line.pop();
      } else {
        // The final cluster is not a single JUSTIFIABLE QUAD
        // character.
        break;
      }
    }
    for (let index = 0; index < line.length;/* do nothing */
    ) {
      // Iterate over the remaining clusters and delete any WORD
      // SEPARATORs.
      const cluster = line[index];
      if (
        cluster.length == 1 &&
        cluster.base == requiredCharacters["WORD SEPARATOR"]
      ) {
        // The current cluster is a WORD SEPARATOR.
        line.splice(index, 1);
      } else {
        // The current cluster is not a WORD SEPARATOR.
        ++index;
      }
    }
    return line;
  };

  /**
   * Yields code·points which represent this node, taking into account
   * paragraphs, lists, ⁊·c.
   *
   * The following kinds of element are currently recognized :—
   *
   * - `<b>`
   * - `<blockquote>`
   * - `<br>`
   * - `<div>`
   * - `<em>`
   * - `<i>`
   * - `<li>`
   * - `<p>`
   * - `<strong>`
   * - `<wbr>`
   *
   * Support for other elements may be added in the future.
   *
   * Proper rendering requires a charset with support for various
   * features, including a mapping for `»` and `•` and compatibility
   * mappings for `https://ns.1024.gdn/Kixt/#BOLDFACE`,
   * `https://ns.1024.gdn/Kixt/#ITALIC`, and
   * `https://ns.1024.gdn/Kixt/#BOLDFACE_ITALIC`. However, any
   * text‐compatible charset may be used.
   *
   * ※ The iterable iterator returned by `charactersFromDomNode` is
   * not “live”; it will yield the same values regardless of any
   * changes to this node after it is called.
   */
  function codepointsFromDomNode(definitions, node) {
    const linebreak = Symbol("linebreak");
    const parentList = Symbol("parent list");
    const ordinalValuesMap = new WeakMap();
    const items = [];
    { // Walk the Dom tree for this node and collect items from it.
      //
      // Items need to be collected before anything is yielded in order
      // to “freeze” the representation.
      const modes = { boldface: false, italic: false };
      const parents = [];
      const walker = node.ownerDocument.createTreeWalker(
        node,
        NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_TEXT,
      );
      processingNodes:
      for (
        let currentNode = walker.currentNode;
        true;
        currentNode = walker.currentNode
      ) {
        // Iterate over the nodes selected by the TreeWalker and use
        // them to build up the items.
        let pushedText = false;
        if (currentNode.nodeType === Node.TEXT_NODE) {
          // The current node is a text node; map it to code·points.
          //
          // All ascii white·space is normalized to JUSTIFIED QUAD.
          //
          // ※ LINE SEPARATOR and PARAGRAPH SEPARATOR are not stripped
          // or collapsed, per Dom behaviours which only match ascii.
          items.push(
            codepointsFromUnicode(
              definitions,
              function* (text) {
                for (
                  const char of text.replaceAll(
                    /[\t\n\f\r\x20]+/gu,
                    " ",
                  )
                ) {
                  // Iterate over each (white·space‐normalized)
                  // character in the current text node and yield its
                  // codepoint.
                  yield char.codePointAt(0);
                }
              }(currentNode.textContent),
              modes.boldface
                ? modes.italic
                  ? `${kixtNamespace}BOLDFACE_ITALIC`
                  : `${kixtNamespace}BOLDFACE`
                : modes.italic
                ? `${kixtNamespace}ITALIC`
                : undefined,
            ),
          );
          pushedText = true;
        } else if (currentNode.namespaceURI === xhtmlNamespace) {
          // The current node is an H·T·M·L element. Perform additional
          // processing…
          switch (currentNode.localName) {
            case "a": {
              // The current node is an `<a>`; mark up the link with
              // tag characters if applicable.
              if (a.hasAttribute("href")) {
                // The current node is an `<a>` with an `@href`.
                items.push([
                  requiredCharacters.REFERENCE,
                  ...Array.from(encodeURI(a.href)).map(($) => {
                    const charCode = $.codePointAt(0);
                    return 0x81A0 + charCode % 0x40 +
                      (charCode / 0x40 >>> 0) * 0x100;
                  }),
                ]);
                pushedText = true;
              } else {
                // The current node is an `<a>` with no `@href`.
                /* do nothing */
              }
              break;
            }
            case "b":
            case "strong": {
              // The current node is a `<b>` or `<strong>` element.
              // Toggle the boldface mode.
              modes.boldface = !modes.boldface;
              break;
            }
            case "blockquote":
            case "li": {
              // The current node is a `<blockquote>` or `<li>`
              // element. Add a line separator and add it to the list
              // of parents.
              parents.push(currentNode);
              items.push({
                [linebreak]: +requiredCharacters["LINE SEPARATOR"],
                [parentList]: [...parents],
              });
              break;
            }
            case "br":
            case "div":
            case "ul": {
              // The current node is a `<br>`, `<div>`, or `<ul>`
              // element. Add a line separator.
              items.push({
                [linebreak]: +requiredCharacters["LINE SEPARATOR"],
                [parentList]: [...parents],
              });
              break;
            }
            case "em":
            case "i": {
              // The current node is an `<em>` or `<i>` element. Toggle
              // the italic mode.
              modes.italic = !modes.italic;
              break;
            }
            case "ol": {
              // The current node is an `<ol>` element. Add a line
              // separator and compute ordinal values for all of its
              // `<li>` children.
              items.push({
                [linebreak]: +requiredCharacters["LINE SEPARATOR"],
                [parentList]: [...parents],
              });
              const reversed = currentNode.hasAttribute("reversed");
              const liChildren = Array.from(currentNode.children)
                .filter(
                  (child) =>
                    child.namespaceURI === xhtmlNamespace &&
                    child.localName === "li",
                );
              let numbering = currentNode.hasAttribute("start")
                ? currentNode.start
                : reversed
                ? liChildren.length
                : 1;
              for (const child of currentNode.children) {
                // Iterate over the `<li>` children, assigning ordinal
                // values.
                if (child.hasAttribute("value")) {
                  // The child has a specified value; set `numbering`
                  // to match.
                  numbering = child.value;
                } else {
                  // The child has no specified value; the value is
                  // `numbering`.
                  /* do nothing */
                }
                ordinalValuesMap.set(child, numbering);
                numbering += reversed ? -1 : 1;
              }
              break;
            }
            case "p": {
              // The current node is a `<p>` element. Add a paragraph
              // separator.
              items.push({
                [linebreak]:
                  +requiredCharacters["PARAGRAPH SEPARATOR"],
                [parentList]: [...parents],
              });
              break;
            }
            case "wbr": {
              // The current node is a `<wbr>` element. Add a word
              // separator.
              items.push([requiredCharacters["WORD SEPARATOR"]]);
              pushedText = true;
              break;
            }
            default: {
              // The current node has no special behaviours associated
              // with it.
              break;
            }
          }
        } else {
          // The current node is not a text node or an HTML element.
          /* do nothing */
        }
        if (pushedText) {
          // Text was pushed during this iteration.
          for (const [index, parent] of parents.entries()) {
            // Iterate over the parents and replace any `<li>`s with
            // null, since `<li>`s should only display their sigil for
            // the first piece of text.
            if (parent?.localName === "li") {
              // The current parent is an `<li>`; replace it with null.
              parents[index] = null;
            } else {
              // This is not an `<li>` parent.
              /* do nothing */
            }
          }
        } else {
          // No text was pushed in this iteration.
          /* do nothing */
        }
        if (walker.firstChild()) {
          // There is a child node to process.
          /* do nothing */
        } else {
          // There are no child nodes to process.
          while (walker.nextSibling() === null) {
            // The parent node has been exhausted; push any number of
            // required linebreaks at the end and prepare for the next
            // node.
            currentNode = walker.parentNode();
            if (currentNode == null) {
              // The entrie tree has been exhausted.
              break processingNodes;
            } else if (currentNode?.namespaceURI === xhtmlNamespace) {
              // There is a parent H·T·M·L element.
              switch (currentNode.localName) {
                case "a": {
                  // The current node is an `<a>`; close the reference if
                  // it is open.
                  //
                  // It is not possible for an `<a>` to be closed as the
                  // first text in a `<li>`, so it’s not necessary to worry
                  // about that here.
                  if (a.hasAttribute("href")) {
                    // This is an `<a>` with an `@href`.
                    items.push([requiredCharacters["CLOSE TAG"]]);
                  } else {
                    // This `<a>` has no `@href`.
                    /* do nothing */
                  }
                  break;
                }
                case "b":
                case "strong": {
                  // The current node is a `<b>` or `<strong>` element.
                  // Toggle the boldface mode.
                  modes.boldface = !modes.boldface;
                  break;
                }
                case "blockquote":
                case "li": {
                  // The current node is a `<blockquote>` or `<li>`
                  // element. Add a line separator and pop the most
                  // recent parent.
                  parents.pop();
                  items.push({
                    [linebreak]: +requiredCharacters["LINE SEPARATOR"],
                    [parentList]: [...parents],
                  });
                  break;
                }
                case "div":
                case "ol":
                case "ul": {
                  // The current node is a `<div>`, `<ol>`, or `<ul>`
                  // element. Add a line separator.
                  items.push({
                    [linebreak]: +requiredCharacters["LINE SEPARATOR"],
                    [parentList]: [...parents],
                  });
                  break;
                }
                case "em":
                case "i": {
                  // The current node is an `<em>` or `<i>` element. Toggle
                  // the italic mode.
                  modes.italic = !modes.italic;
                  break;
                }
                case "p": {
                  // The current node is a `<p>` element. Add a
                  // paragraph separator.
                  items.push({
                    [linebreak]:
                      +requiredCharacters["PARAGRAPH SEPARATOR"],
                    [parentList]: [...parents],
                  });
                  break;
                }
                default: {
                  // The current node has no special behaviours
                  // associated with it.
                  break;
                }
              }
            } else {
              /* do nothing */
            }
          }
        }
      }
    }
    return function* () {
      // Process the resulting items and yield codepoints.
      const context = Object.preventExtensions({
        [linebreak]: null, // `null` is a special value
        [parentList]: [],
        pendingSpace: false,
        linebreakToYield: null,
        sigilsToYield: [],
        yieldedCodepoint: false,
      });
      for (const item of items) {
        // Iterate over each item and process the result.
        const {
          [linebreak]: linebreakCodepoint,
          [parentList]: parents,
        } = context;
        if (linebreak in item) {
          // The item specifies a line·break and a current list of
          // parent nodes.
          if (linebreakCodepoint === null) {
            // Nothing has been yielded yet; no line·breaks are
            // necessary but keep track of the current list of parents.
            context[parentList] = item[parentList];
          } else {
            // Choose the linebreak character with the smallest
            // codepoint (highest priority), and update the list of
            // parents.
            context[linebreak] = Math.min(
              linebreakCodepoint || Infinity,
              item[linebreak],
            );
            context[parentList] = item[parentList];
            context.sigilsToYield = []; // reset
          }
        } else {
          // The item is an iterable of code·points; yield the required
          // line·break and sigils, then yield the item.
          //
          // After this process, both the required line·break and the
          // parents are reset. Parents are only restored after a
          // required line·break is specified, so (for example) they
          // will not be re·added between two adjacent <span>s (this is
          // desired behaviour).
          { // Process the required line·break.
            if (linebreakCodepoint != null) {
              // A line·break is required before additional content;
              // yield it first. Reset `pendingSpace` and
              // `yieldedCodepoint` in the context as spaces are
              // trimmed after mandatory line·breaks.
              if (context.yieldedCodepoint) {
                // A code·point has been yielded since the last
                // required line·break, so the line·break can be
                // yielded.
                context.linebreakToYield = linebreakCodepoint;
                context.yieldedCodepoint = false;
              } else {
                // No code·point has been yielded since the last
                // required line·break, so the line·break with the
                // highest priority must be chosen.
                context.linebreakToYield = Math.min(
                  linebreakCodepoint,
                  context.linebreakToYield || Infinity,
                );
              }
              context.pendingSpace = false;
            } else {
              // No line·breaks are required presently.
              /* do nothing */
            }
            context[linebreak] = undefined; // not `null`
          }
          { // Process the parents.
            const { sigilsToYield } = context;
            const { length } = sigilsToYield;
            for (const parent of parents) {
              // Iterate over the parents and yield code·points as
              // necessary.
              //
              // ※ There won’t be any parents if there wasn’t a
              // required line·break immediately preceding the current
              // item.
              if (parent == null) {
                // The current parent is null; yield spacing.
                sigilsToYield.splice(
                  length,
                  0,
                  ...codepointsFromUnicode(
                    definitions,
                    [0xA0, 0xA0],
                  ),
                );
              } else {
                // The current parent is an element; yield an
                // appropriate sigil.
                const ordinalValue = ordinalValuesMap.get(parent);
                if (ordinalValue != null) {
                  // The current parent has an associated ordinal
                  // value; yield the corresponding “number full stop”,
                  // falling back to yielding the corresponding number
                  // and then a full stop if the former results in the
                  // replacement character.
                  //
                  // ※ This implies that it is an `<li>` inside of an
                  // `<ol>` which was touched by the TreeWalker above.
                  // If the root node is an `<li>` element which is
                  // inside an `<ol>`, it will still not have an
                  // ordinal value assigned and instead will render as
                  // if it were in a `<ul>`.
                  const replacement = +requiredCharacters.REPLACEMENT;
                  const codepoints =
                    ordinalValue >= 1 && ordinalValue <= 20
                      ? [...codepointsFromUnicode(
                        definitions,
                        [0x2487 + ordinalValue, 0xA0],
                      )]
                      : [replacement];
                  sigilsToYield.splice(
                    length,
                    0,
                    ...(codepoints[0] === replacement
                      ? codepointsFromUnicode(
                        definitions,
                        function* () {
                          for (const char of `${ordinalValue}.\xA0`) {
                            yield char.codePointAt(0);
                          }
                        }(),
                      )
                      : codepoints),
                  );
                } else {
                  // The current parent does not have an associated
                  // ordinal value.
                  switch (parent.localName) {
                    case "blockquote": {
                      // The current parent is a `<blockquote>`; yield
                      // ‹ » ›.
                      sigilsToYield.splice(
                        length,
                        0,
                        ...codepointsFromUnicode(
                          definitions,
                          [0xBB, 0xA0],
                        ),
                      );
                      break;
                    }
                    case "li": {
                      // The current parent is an `<li>`; yield a
                      // bullet.
                      sigilsToYield.splice(
                        length,
                        0,
                        ...codepointsFromUnicode(
                          definitions,
                          [0x2022, 0xA0],
                        ),
                      );
                      break;
                    }
                    default: {
                      // The current parent is not recognized; fall
                      // back to REPLACEMENT.
                      sigilsToYield.splice(
                        length,
                        0,
                        ...codepointsFromUnicode(
                          definitions,
                          [0xFFFD, 0xA0],
                        ),
                      );
                      break;
                    }
                  }
                }
              }
            }
            context[parentList] = [];
          }
          { // Yield the code·points.
            //
            // JUSIFIABLE QUAD spaces are only yielded if they precede
            // another character (prior to a mandatory line·break) and
            // are not the first character (after a mandatory }
            // line·break).
            for (const codepoint of item) {
              // Iterate over the code·points and yield them as
              // necessary.
              if (
                codepoint == requiredCharacters["JUSTIFIABLE QUAD"]
              ) {
                // The current code·point is a space.
                context.pendingSpace = context.yieldedCodepoint;
              } else {
                // The current code·point is not a space.
                const { linebreakToYield, sigilsToYield } = context;
                if (linebreakToYield != null) {
                  // There is a pending required line·break to yield.
                  yield linebreakToYield;
                  context.linebreakToYield = null;
                } else {
                  // There were no line·breaks to yield.
                  /* do nothing */
                }
                if (sigilsToYield.length > 0) {
                  // There are pending sigils to yield.
                  yield* sigilsToYield;
                  context.sigilsToYield = [];
                } else {
                  // There were no sigils to yield.
                  /* do nothing */
                }
                if (context.pendingSpace) {
                  // A space character is needed.
                  yield +requiredCharacters["JUSTIFIABLE QUAD"];
                  context.pendingSpace = false;
                } else {
                  // No space character is needed.
                  /* do nothing */
                }
                yield codepoint;
                context.yieldedCodepoint = true;
              }
            }
          }
        }
      }
    }();
  }

  /**
   * Yields code·points in the provided code·point definitions object
   * which correspond to the provided iterable of Unicode code·points.
   *
   * If a third argument is provided, the resulting code·points will be
   * remapped onto those code·points which have compatibility mappings
   * of the provided mode, should they exist.
   *
   * If no suitable mapping can be found, Unicode characters will map
   * to REPLACEMENT.
   *
   * ※ The first argument is a code·point definitions object, not a
   * charset, and the second is an iterable of code·points, not
   * characters.
   *
   * ※ This function yields code·points because that is the input of
   * the sanitization function.
   *
   * ※ If it is possible to map a sequence of Unicode code·points to a
   * single code·point in the provided charset, this function will
   * attempt to do so. However, where multiple valid mappings exist, it
   * cannot guarantee producing the smallest.
   *
   * ☡ This function assumes, but does not ensure, that the provided
   * charset definitions are text‐compatible.
   */
  function* codepointsFromUnicode(
    definitions,
    unicodeCodepoints,
    mode,
  ) {
    const compatibilityMode = mode == null ? null : `${mode}`;
    const { unicodeTree, mappingsForMode } = (() => {
      if (unicodeContextForDefinitions.has(definitions)) {
        // There is an existing Unicode context for this code·point
        // definitions object; re·use it.
        const {
          unicodeTree,
          modeMappings,
        } = unicodeContextForDefinitions.get(definitions);
        return {
          unicodeTree,
          mappingsForMode: compatibilityMode == null
            ? null
            : compatibilityMode in modeMappings
            ? modeMappings[compatibilityMode]
            : Object.create(null),
        };
      } else {
        // There is no existing Unicode context for this code·point
        // definitions object; generate one from the definitions.
        const unicodeTree = Object.values(requiredCharacters).reduce(
          (tree, char) => {
            // This reduce function can take shortcuts given that
            // `requiredCharacters` only consists of characters which
            // map to single valid Unicode code·points.
            const code = +char;
            const uni = +char.unicode[0];
            if (uni in tree) {
              // There is a previous character with this Unicode
              // mapping.
              /* do nothing */
            } else {
              // There is no previous character with this Unicode
              // mapping.
              tree[uni] = Object.assign(
                Object.create(null),
                { [treeValue]: code },
              );
            }
            return tree;
          },
          Object.create(null),
        );
        const modeMappings = Object.create(null);
        unicodeContextForDefinitions.set(definitions, {
          unicodeTree,
          modeMappings,
        });
        for (const character of Object.values(definitions)) {
          // Iterate over each code·points in the definitions object
          // and organize it into a tree based on its Unicode mappings.
          const { unicode } = character;
          const codepoint = +character;
          if (unicode == null) {
            // There is no Unicode mapping for this character.
            //
            // This isn’t a welformed charset, but for the sake of this
            // algorithm just skip the character.
            /* do nothing */
          } else {
            // There is a Unicode mapping for this character.
            const mappingCodepoints = Array.from(unicode, ($) => +$);
            try {
              // Ensure that the mapping code·points are valid Unicode.
              if (mappingCodepoints.length) {
                // At least one mapping code·point is provided.
                String.fromCodePoint(...mappingCodepoints);
              } else {
                // No mapping code·points are provided.
                throw null;
              }
            } catch {
              // The mapping code·points weren’t valid Unicode.
              throw new TypeError(
                `Kijch: Invalid unicode mapping for codepoint: ${
                  codepoint.toString(16).toUpperCase().padStart(4, "0")
                }.`,
              );
            }
            setTreeValueIfUndefined(
              unicodeTree,
              mappingCodepoints,
              codepoint,
            );
            if (character.compatibilityMode != null) {
              // The current character has a compatibility mode; define
              // the reverse mapping if possible.
              const mappingMode = `${character.compatibilityMode}`;
              const mappingTree = mappingMode in modeMappings
                ? modeMappings[mappingMode]
                : (modeMappings[mappingMode] = Object.create(null));
              const { compatibility } = character;
              if (compatibility == null) {
                // There is no compatibility mapping for this
                // character.
                //
                // This isn’t a welformed charset, but for the sake of
                // this algorithm just skip the mapping.
                /* do nothing */
              } else {
                // There is a compatibility mapping for this character.
                const mappingCodepoints = Array.from(
                  compatibility,
                  ($) => +$,
                );
                if (
                  !mappingCodepoints.length ||
                  mappingCodepoints.some(($) =>
                    !($ >= 0 && $ <= 0xFFFF)
                  )
                ) {
                  // The mapping code·points aren’t valid.
                  throw new TypeError(
                    `Kijch: Invalid compatibility mapping for codepoint: ${
                      codepoint.toString(16).toUpperCase().padStart(
                        4,
                        "0",
                      )
                    }.`,
                  );
                } else {
                  setTreeValueIfUndefined(
                    mappingTree,
                    mappingCodepoints,
                    codepoint,
                  );
                }
              }
            } else {
              // The current character does not have a compatibility
              // mode defined.
            }
          }
        }
        return {
          unicodeTree,
          mappingsForMode: compatibilityMode == null
            ? null
            : compatibilityMode in modeMappings
            ? modeMappings[compatibilityMode]
            : Object.create(null),
        };
      }
    })();
    const unmapped = mapCodepoints(
      unicodeTree,
      unicodeCodepoints,
      +requiredCharacters.REPLACEMENT,
    );
    if (compatibilityMode == null) {
      // There is no compatibility mode; the unmapped codepoints can be
      // yielded directly.
      yield* unmapped;
    } else {
      // There is a compatibility mode, so a repeated mapping process
      // needs to be undertaken.
      yield* mapCodepoints(mappingsForMode, unmapped);
    }
  }

  /**
   * Yields codepoints generated by mapping the provided codepoints
   * iterable according to the provided tree of mappings.
   *
   * If a third argument is provided, it is yielded when no mapping is
   * found. Otherwise, codepoints fall back to mapping to themselves.
   */
  function* mapCodepoints(mapTree, codepoints, initialValue = null) {
    const iterator = new PeekableIterator(
      codepoints,
      ($) => +$,
    );
    for (const codepoint of iterator) {
      // Iterate over all the codepoints and map them where possible.
      let bestValue = initialValue ?? codepoint;
      let peekDistanceOfBestMatch = 0;
      for (
        let searchIndex = 0,
          searchCodepoint = codepoint,
          treeNode = mapTree;
        (treeNode = treeNode[searchCodepoint]);
        searchCodepoint = iterator.peek(++searchIndex)
      ) {
        // Dig into the compatibility mapping tree, updating the
        // `bestValue` whenever a code·point is defined for the
        // current value.
        //
        // Not all nodes will necessarily have associated code·point
        // values. For example, if there are characters with mappings
        // for `b` and `bar`; there will be a node but no mapping for
        // `ba`, which will be encountered when looking up the
        // mapping for `baz`.
        if (treeValue in treeNode) {
          // There is a value for the node; update `bestValue` and
          // the `index`.
          //
          // ※ The index is set to the index of the final character
          // being mapped; it still needs to be incremented before the
          // next iteration.
          bestValue = treeNode[treeValue];
          peekDistanceOfBestMatch = searchIndex;
        } else {
          // There is no value for the node.
          /* do nothing */
        }
      }
      iterator.drop(peekDistanceOfBestMatch);
      yield bestValue;
    }
  }

  /** A symbol which identifies marker objects. */
  const markerType = Symbol("marker type");

  /**
   * Yields the characters in the provided code·point definitions
   * object which are specified by the provided code·points, with
   * invalid or undefined sequences replaced by replacement characters.
   *
   * ☡ Null characters will be dropped silently during sanitization.
   *
   * ※ The first argument is a code·point definitions object, not a
   * charset, and the second is an iterable of code·points, not
   * characters.
   *
   * ※ Because `sanitized` assumes a text context, all control and
   * messaging characters are treated as invalid.
   *
   * ☡ `sanitized` will throw an error if the provided charset is not
   * text‐compatible.
   */
  function* sanitized(definitions, codepoints) {
    const objectCodepoint = requiredCharacters.OBJECT.codepoint;
    const replacementCodepoint =
      requiredCharacters.REPLACEMENT.codepoint;
    let index = -1;
    const iterator = new PeekableIterator(codepoints, ($) => +$);
    processingCodepoints:
    for (const codepoint of iterator) {
      // Iterate over each code·point in the provided code·points, and
      // sanitize them accordingly.
      ++index;
      if (codepoint !== codepoint | 0 || codepoint > 0xFFFF) {
        // The code·point does not have a numeric value between 0 and
        // 0xFFFF inclusive.
        throw new RangeError(
          `Kijch: Code·point out of range at position ${index}: ${codepoint}.`,
        );
      } else if (!(codepoint in definitions)) {
        // The current code·point is not in the charset; replace it.
        yield definitions[replacementCodepoint];
        continue processingCodepoints;
      } else if (codepoint === 0) {
        // The current code·point is null; drop it silently.
        continue processingCodepoints;
      } else {
        // The current code·point is a non‐null code·point with a
        // definition in the provided charset.
        const definition = definitions[codepoint];
        switch (
          definition?.basicType ?? `${kixtNamespace}UNASSIGNED`
        ) {
          case `${kixtNamespace}FORMAT`:
          case `${kixtNamespace}NONSPACING`:
          case `${kixtNamespace}SPACING`:
          case `${kixtNamespace}PRIVATEUSE`:
          case `${kixtNamespace}NONCHARACTER`: {
            // The current code·point is a format, nonspacing, spacing,
            // private·use, or noncharacter code·point; yield it.
            yield definition;
            continue processingCodepoints;
          }
          case `${kixtNamespace}DATA`: {
            // Data sequences are not supported (yet), so they are all
            // replaced.
            //
            // This still requires actually *processing* the data
            // sequences, so this code is nevertheless complex.
            let peekIndex = 1;
            if (
              codepoint == requiredCharacters.DATA &&
              iterator.peek(peekIndex) != requiredCharacters.LEAVE
            ) {
              // The current code·point is DATA and is not followed by
              // LEAVE; i·e it is a data sequence beginning with a
              // media type.
              --peekIndex; // reset to 0 before this loop
              while (true) {
                // Read the media type.
                const mediaTypeCodepoint = iterator.peek(++peekIndex);
                if (
                  mediaTypeCodepoint ==
                    requiredCharacters["STRING BEGIN"]
                ) {
                  // The media type has finished; see below for further
                  // processing.
                  break;
                } else if (
                  mediaTypeCodepoint == null ||
                  mediaTypeCodepoint >= 0x7F ||
                  mediaTypeCodepoint <= 0x20
                ) {
                  // There was an unexpected character or E·O·F while
                  // processing the media type; the DATA is invalid.
                  yield definitions[replacementCodepoint];
                  continue processingCodepoints;
                } else {
                  // The current character is a valid media type
                  // character.
                  continue;
                }
              }
            } else if (
              codepoint == requiredCharacters.DATA ||
              codepoint == requiredCharacters.SHIFT ||
              codepoint == requiredCharacters["SHIFT STRING"]
            ) {
              // The current code·point is one which must be followed
              // by LEAVE or data.
              if (
                codepoint == requiredCharacters.DATA &&
                iterator.peek(peekIndex) != requiredCharacters.LEAVE
              ) {
                // The current code·point must be followed by LEAVE and
                // is not, so is thus invalid.
                yield definitions[replacementCodepoint];
                continue processingCodepoints;
              } else if (
                iterator.peek(peekIndex) == requiredCharacters.LEAVE
              ) {
                // The current code·point is followed by LEAVE.
                while (true) {
                  // Read the I·R·I.
                  const iriCodepoint = iterator.peek(++peekIndex);
                  if (iriCodepoint == null) {
                    // There was an E·O·F while processing the I·R·I;
                    // the current code·point is invalid.
                    yield definitions[replacementCodepoint];
                    continue processingCodepoints;
                  } else if (
                    iriCodepoint == requiredCharacters["RETURN"]
                  ) {
                    // The current character is RETURN; see below for
                    // further processing.
                    break;
                  } else {
                    // The current character is not RETURN and thus an
                    // I·R·I character.
                    continue;
                  }
                }
              } else {
                // The current character is not followed by LEAVE (and
                // thus has no I·R·I), but this is allowed.
                --peekIndex; // reset to zero
              }
              { // `peekIndex` is now just before the start of data.
                const nextCodepoint = iterator.peek(++peekIndex);
                if (nextCodepoint == null) {
                  // There is no character following the opener; the
                  // current code·point is invalid.
                  yield definitions[replacementCodepoint];
                  continue processingCodepoints;
                } else if (codepoint == requiredCharacters.SHIFT) {
                  // The current code·point is SHIFT; and the I·R·I is
                  // followed by a character; consume the data sequence
                  // and return a single replacement code·point.
                  iterator.drop(peekIndex);
                  yield definitions[replacementCodepoint];
                  continue processingCodepoints;
                } else if (
                  nextCodepoint == requiredCharacters["STRING BEGIN"]
                ) {
                  // The current code·point opens a data string; see
                  // below for processing thereof.
                  /* do nothing */
                } else {
                  // The current code·point is invalid.
                  yield definitions[replacementCodepoint];
                  continue processingCodepoints;
                }
              }
            } else if (
              codepoint == requiredCharacters["STRING BEGIN"]
            ) {
              // The current code·point is STRING BEGIN; see below for
              // processing.
              --peekIndex;
              break;
            } else {
              // The current code·point is an unrecognized data
              // character.
              yield definitions[replacementCodepoint];
              continue processingCodepoints;
            }
            { // `peekIndex` is now necessarily at STRING BEGIN; read a
              // data string.
              let dataCodepoint;
              while (
                (dataCodepoint = iterator.peek(++peekIndex)) != null
              ) {
                if (
                  dataCodepoint == requiredCharacters["STRING FINISH"]
                ) {
                  // The current data code·point is STRING FINISH;
                  // check the number of DONTs and handle accordingly.
                  let dontIndex = peekIndex;
                  while (--dontIndex >= 0) {
                    // Scan backwards, counting DONTs.
                    if (
                      iterator.peek(dontIndex) !=
                        requiredCharacters.DONT
                    ) {
                      // The current character is not a DONT; they’ve
                      // all been counted.
                      break;
                    } else {
                      // The current character is a DONT.
                      continue;
                    }
                  }
                  ++dontIndex; // reset to index of last DONT
                  if ((peekIndex - dontIndex) % 2 === 0) {
                    // The number of DONTs is even; consume the data
                    // sequence and yield an object replacement.
                    iterator.drop(peekIndex);
                    yield definitions[objectCodepoint];
                    continue processingCodepoints;
                  } else {
                    // The number of DONTs is even; continue processing
                    // the data string.
                    continue;
                  }
                } else {
                  // The current character is something other than
                  // STRING FINISH.
                  continue;
                }
              }
              { // The string was never terminated, so this codepoint
                // is invalid.
                index = startOfDataSequence;
                yield definitions[replacementCodepoint];
                continue processingCodepoints;
              }
            }
          }
          default: {
            // The current code·point is some other kind of code·point
            // (and thus unsupported in a text context).
            yield definitions[replacementCodepoint];
            continue processingCodepoints;
          }
        }
      }
    }
  }

  /**
   * Digs into the provided tree to the location specified by the
   * provided path, and sets its `[treeValue]` to the provided value
   * if it is not already defined, creating new nodes as necessary to
   * accomplish this.
   */
  const setTreeValueIfUndefined = (tree, path, value) => {
    let treeNode = tree;
    for (const pathComponent of path) {
      // Dig into the tree to the correct location.
      treeNode = pathComponent in treeNode
        ? treeNode[pathComponent]
        : (treeNode[pathComponent] = Object.create(null));
    }
    if (!(treeValue in treeNode)) {
      // A value has not already been assigned for the
      // corresponding node.
      treeNode[treeValue] = value;
    } else {
      // A value has already been assigned for the
      // corresponding node.
      /* do nothing */
    }
  };

  /**
   * Throws if the provided charset is not text‐compatible; otherwise,
   * returns an object mapping code·points to characters.
   *
   * ※ The return value of this function is an ordinary object, not a
   * charset.
   */
  const textCompatibleDefinitions = (charset) => {
    const definitions = Object.create(null);
    for (const definition of charset) {
      // Iterate over the characters in the charset and add them to
      // the definitions object by codepoint.
      const codepoint = Character.prototype.valueOf.call({
        codepoint: definition,
      });
      if (codepoint in definitions) {
        // The code·point was already defined in the provided charset.
        throw new TypeError(
          `Kijch: Duplicate code·point in charset: ${codepoint}.`,
        );
      } else {
        // The code·point has not already been defined in the provided
        // charset.
        definitions[codepoint] = definition;
      }
    }
    for (const character of Object.values(requiredCharacters)) {
      // Iterate over each character in `requiredCharacters` and ensure
      // that a compatible definition exists in the provided charset.
      const codepoint = +character;
      const charsetCharacter = definitions[codepoint];
      if (charsetCharacter != null) {
        // A character with the same code·point as the required
        // character is defined.
        if (
          Character.prototype.compatibleWith.call(
            character,
            charsetCharacter,
          )
        ) {
          // The defined character is compatible with the required
          // character.
          continue;
        } else {
          // The defined character is not compatible with the required
          // character.
          throw new TypeError(
            `Kijch: Charset properties for code·point do not match the required compatibility properties: ${
              codepoint.toString(16).toUpperCase().padStart(4, "0")
            }.`,
          );
        }
      } else {
        // No character with the same code·point as the required
        // character is defined.
        throw new TypeError(
          `Kijch: Charset is missing required codepoint: ${
            codepoint.toString(16).toUpperCase().padStart(4, "0")
          }.`,
        );
      }
    }
    return definitions;
  };

  /**
   * A symbol used to get the value of a tree element within a unicode
   * mapping tree.
   */
  const treeValue = Symbol("tree value");

  /**
   * A WeakMap associating a given code·point definitions object with
   * an object describing its Unicode mappings.
   *
   * ※ This allows the same context object to be re·used within a
   * single iterator (i·e across multiple calls to
   * `charactersFromUnicode`). It does **not** allow the same context
   * object to be re·used *across* iterators, as each iterator
   * constructs its own charset definition object (which only lasts as
   * long as the iterator does).
   */
  const unicodeContextForDefinitions = new WeakMap();

  /**
   * Yields non·ignorable characters from the provided text,
   * interspersed with markers representing the beginning and ending of
   * various kinds of span.
   *
   * Right now only tag sequences are supported, but other kinds of
   * span are planned.
   */
  const withoutIgnorables = function* (text) {
    let lastCharacter = null;
    let currentTag = null;
    for (const character of text) {
      // Iterate over the characters in the text and process each.
      const codepoint = +character;
      { // Process tags.
        if (
          codepoint >= 0x81C0 && codepoint <= 0x81DF ||
          codepoint >= 0x82A0 && codepoint <= 0x82DE
        ) {
          // The current character is a tag character; collect it into
          // the current tag.
          const tagAscii = String.fromCodePoint(
            codepoint <= 0x81DF
              ? codepoint - 0x81A0
              : codepoint - 0x8260,
          );
          if (currentTag != null) {
            // A tag is currently open; append the corresponding ascii
            // character to its value.
            currentTag.value += tagAscii;
          } else {
            // A tag is not currently open; initialize a new one.
            currentTag = {
              [markerType]: "tag",
              identification: lastCharacter,
              value: tagAscii,
            };
          }
        } else if (currentTag != null) {
          // The current character is not a tag character, but a tag is
          // open; yield it and close it out.
          yield currentTag;
          currentTag = null;
        } else {
          // The current character is not a tag character and a tag is
          // not open.
          /* do nothing */
        }
      }
      { // Yield a character/marker if necessary.
        if (codepoint == requiredCharacters["CLOSE TAG"]) {
          // The current character is CLOSE TAG; yield a close tag
          // marker.
          yield {
            [markerType]: "close tag",
            identification: lastCharacter,
          };
        } else if (
          codepoint in requiredCharacters &&
          requiredCharacters[codepoint].ignorable
        ) {
          // The current character is ignorable.
          /* do nothing */
        } else {
          // The current character is not ignorable; yield it.
          yield character;
        }
      }
      lastCharacter = character;
    }
  };

  class Text extends Span {
    /** A brand identifying `Text`s created using the constructor. */
    #isText = true;

    /**
     * Constructs a new `Text` from the provided generator of
     * characters.
     *
     * Characters are accessed lazily but then cached, so the first
     * iteration of the resulting `Text` will likely be slower that
     * later iterations. (This works even if multiple iterators are
     * running at different times and speeds.)
     *
     * ※ This constructor is not exposed.
     */
    constructor(characters) {
      const iterator = characters;
      const cache = [];
      let done = false;
      super(
        function* () {
          if (done) {
            // The character iterator has completed; simply yield the
            // characters which have already been gathered.
            yield* cache;
          } else {
            // The character iterator has not yet completed. Iterate
            // manually and gather new characters as necessary.
            let index = 0;
            for (; !done; ++index) {
              // Increment `index` and gather new characters as needed
              // until the iterator is exhausted.
              if (index < cache.length) {
                // `index` points to a character in the cache.
                yield cache[index];
              } else {
                // `index` points to a character not yet cached.
                const nextResult = iterator.next();
                if (nextResult.done) {
                  // The iterator has been exhausted.
                  done = true;
                } else {
                  // The iterator containts another character; cache
                  // and yield it.
                  const { value: character } = nextResult;
                  cache.push(character);
                  yield character;
                }
              }
            }
            yield* cache.slice(index);
          }
        },
        Object.create(textPrototype),
      );
    }

    /**
     * Yields the `Cluster`s of this text.
     *
     * ※ Not all characters in a text will be grouped into its
     * clusters; it is not necessarily possible to reconstruct a given
     * text from the clusters it produces. In particular, “ignorable”
     * characters, which are not meant to influence display, are not
     * directly present in the clusters of a text, although they may
     * influence the clusters in other ways (for example by defining
     * spans).
     *
     * ※ Linebreak characters always form a solitary cluster.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    *clusters() {
      const currentGroup = [];
      const tags = new Map();
      const nextTags = new Map();
      const groupCluster = () => {
        const { length } = currentGroup;
        const result = (() =>
          length
            ? [
              new Cluster(
                currentGroup.splice(0, length),
                [...tags.entries()],
              ),
            ]
            : [])();
        tags.clear();
        for (const [codepoint, tag] of nextTags) {
          // Iterate over the pending tags and add them to the tags
          // list.
          //
          // ※ This function is always called either immediately
          // preceding a new base character or at the end of the
          // string, so this is the opportune time to freeze the tags
          // for the next `Cluster`.
          tags.set(codepoint, tag);
        }
        return result;
      };
      for (const charOrMarker of withoutIgnorables(this)) {
        // Iterate over the markers and non·ignorable characters of the
        // text and yield the resulting clusters.
        if (markerType in charOrMarker) {
          // The current value is a marker.
          switch (charOrMarker[markerType]) {
            case "tag": {
              // The current marker is a tag marker; open a corresponding
              // tag span.
              const { identification, value } = charOrMarker;
              const idCodepoint = identification == null
                ? null
                : +identification;
              if (nextTags.has(idCodepoint)) {
                // There is a previous tag with the same identification;
                // remove it.
                nextTags.delete(idCodepoint);
              } else {
                // There is no previous tag with the same identification.
                /* do nothing */
              }
              nextTags.set(idCodepoint, [identification, value]);
              continue;
            }
            case "close tag": {
              // The current marker is a close tag marker; close the
              // appropriate tag span(s).
              const { identification } = charOrMarker;
              const idCodepoint = identification == null
                ? null
                : +identification;
              if (nextTags.has(idCodepoint)) {
                // There is a previous tag with the same identification;
                // remove it.
                nextTags.delete(idCodepoint);
              } else {
                // There is no previous open tag with the same
                // identification; remove every tag instead.
                //
                // This behaviour is very weird and possibly bad, but it
                // is the most generous reading of the Unicode spec
                // regarding the CLOSE TAG character. In general, it is a
                // bad idea to have multiple tags open at the same time!
                nextTags.clear();
              }
              continue;
            }
          }
        } else {
          // The current value is a character.
          const codepoint = +charOrMarker;
          const { basicType } = charOrMarker;
          const requiredDescription = codepoint in requiredCharacters
            ? requiredCharacters[codepoint]
            : Object.create(null);
          if (
            requiredDescription.linebreakAfter === true ||
            codepoint == requiredCharacters["WORD SEPARATOR"]
          ) {
            // This is a line·break or word separator character; yield
            // the current group, then the character.
            yield* groupCluster();
            currentGroup.push(charOrMarker);
            yield* groupCluster();
          } else if (
            "fullwidth" in requiredDescription ||
            basicType == `${kixtNamespace}SPACING` ||
            basicType == `${kixtNamespace}PRIVATEUSE`
          ) {
            // This is a spacing character; yield the current group and
            // start a new one with the current character.
            yield* groupCluster();
            currentGroup.push(charOrMarker);
          } else {
            // This is a nonspacing character; add it to the current
            // group.
            currentGroup.push(charOrMarker);
          }
        }
      }
      yield* groupCluster();
    }

    /**
     * Breaks the text up into lines and yields arrays of resulting
     * clusters.
     *
     * The characters in requiredCharacters with a `linebreakAfter` of
     * `true` will be treated as “hard” line breaks. Otherwise, lines
     * will only contain so many glyphs as to be at most the provided
     * (second argument) number of pixels wide.
     *
     * The inferred size of clusters will match the width of the
     * `ImageData` object returned by the `Cluster::toImageData`
     * method, when called with the provided size (first argument) and
     * options (third argument).
     *
     * If a line ends in a hard break, the corresponding character can
     * be accessed from the `break` property on the resulting line
     * array. This property will have a value of `null` for soft
     * breaks, and `undefined` for the final line.
     *
     * ※ This function is intentionally generic; it works for any
     * iterable of characters.
     */
    *lines(fontSize, lineWidth, options) {
      const size = (() => {
        const result = +fontSize;
        return Number.isNaN(result) ? 0 : Math.min(
          Math.max(result, 0),
          0x3FFF,
        );
      })();
      const maxLineWidth = (() => {
        const result = +lineWidth;
        return Number.isNaN(result) ? 0 : Math.max(result, 0);
      })();
      const fullwidth = !!options.fullwidth;
      const currentLine = [];
      let currentWidth = 0;
      for (const cluster of this.clusters()) {
        // Iterate over the clusters and yield lines.
        const base = Reflect.get(Cluster.prototype, "base", cluster);
        const codepoint = +base;
        if (requiredCharacters[codepoint]?.linebreakAfter === true) {
          // The current cluster is a line·break character.
          const result = currentLine.splice(0, currentLine.length);
          currentWidth = 0;
          yield Object.assign(
            cleanLine(result.map(($) => $.cluster)),
            { break: base },
          );
          continue;
        } else if (codepoint == requiredCharacters["WORD SEPARATOR"]) {
          // The current cluster is a WORD SEPARATOR.
          currentLine.push({ cluster, glyphWidth: 0 });
        } else {
          // The current cluster is not a separator character.
          const ensWide = base.basicType == `${kixtNamespace}FORMAT`
            ? 1 +
              !!(requiredCharacters[codepoint]?.fullwidth ?? fullwidth)
            : 1 + !!(base.fullwidth ?? fullwidth);
          const glyphWidth = Math.floor(size / (3 - ensWide));
          currentLine.push({ cluster, glyphWidth });
          currentWidth += glyphWidth;
          if (currentWidth > maxLineWidth) {
            // The current width exceeds the provided maximum.
            if (
              cluster.length == 1 &&
              codepoint == requiredCharacters["JUSTIFIABLE QUAD"]
            ) {
              // The current character is a space character; continue
              // gobbling until a nonspace character is consumed.
              continue;
            } else {
              // The current character is not a space character.
              const breakPos = (() => {
                for (
                  let position = currentLine.length - 1;
                  position > 0;
                  --position
                ) {
                  // Iterate over the clusters in the current line and
                  // find the first for which a preceding line·break
                  // would bring the line back within the provided line
                  // length.
                  //
                  // ※ It is not necessary to actually calculate the
                  // line length each iteration, since it is known that
                  // the line was short enough prior to the last
                  // nonspace character (and line·breaks will never be
                  // inserted between spaces).
                  const leftBase =
                    currentLine[position - 1].cluster.base;
                  const rightBase = currentLine[position].cluster.base;
                  const {
                    linebreakCategory: leftCategory,
                    linebreakAfter: leftClass,
                  } = requiredCharacters[+leftBase] ?? leftBase;
                  const {
                    linebreakCategory: rightCategory,
                    linebreakBefore: rightClass,
                  } = requiredCharacters[+rightBase] ?? rightBase;
                  if (
                    leftClass == `${kixtNamespace}JOIN` ||
                    rightClass == `${kixtNamespace}JOIN` ||
                    leftClass != `${kixtNamespace}BREAK` &&
                      rightClass != `${kixtNamespace}BREAK` &&
                      leftCategory == rightCategory &&
                      (leftClass == `${kixtNamespace}INSEPARABLE` ||
                        rightClass == `${kixtNamespace}INSEPARABLE`)
                  ) {
                    // A line·break is not allowed according to
                    // line‐breaking rules at this position.
                    continue;
                  } else {
                    // A line·break is permissible at this position.
                    return position;
                  }
                }
                return currentLine.length - 1;
              })();
              const result = currentLine.splice(0, breakPos || 1);
              for (const { glyphWidth } of result) {
                currentWidth -= glyphWidth;
              }
              yield Object.assign(
                cleanLine(result.map(($) => $.cluster)),
                { break: null },
              );
              continue;
            }
          } else {
            continue;
          }
        }
      }
      yield Object.assign(
        cleanLine(currentLine.map(($) => $.cluster)),
        { break: undefined },
      );
    }
  }
  const textConstructor = class extends spanConstructor {
    /**
     * Constructs a new `Text` containing the characters in the
     * provided charset indicated by the provided (iterable)
     * code·points.
     */
    constructor(charset, codepoints) {
      return new Text(charactersFromCodepoints(charset, codepoints));
    }

    /**
     * Returns a new `Text` consisting of characters in the provided
     * charset corresponding to the provided Dom node.
     *
     * ☡ The resulting `Text` is not “live”; you will need to create a
     * new `Text` for each Dom update.
     */
    static fromDomNode(charset, node) {
      return new Text(charactersFromDomNode(charset, node));
    }

    /**
     * Returns a new `Text` consisting of characters in the provided
     * charset corresponding to the provided string or iterable of
     * Unicode code·points.
     */
    static fromUnicode(charset, stringOrIterable) {
      return new Text(charactersFromString(charset, stringOrIterable));
    }
  };
  Object.defineProperties(textConstructor, {
    name: { value: "Text" },
  });
  const textPrototype = Object.defineProperties(
    textConstructor.prototype,
    Object.assign(
      Object.getOwnPropertyDescriptors(Text.prototype),
      { constructor: { value: textConstructor } },
    ),
  );
  return { textConstructor };
})();
export { textConstructor as Text };
