# 🔤🟠 Kijch

## What is this?

🔤🟠 Kijch is a (somewhat messy, but hopefully not broken) working
implementation of [Kixt Charsets][Kixt Charsets] and related standards
for Ecmascript. Aside from powering webbased charset viewers and tools,
the hope is for this library to provide a minimal text rendering engine
for HTML canvas applications, and specific methods have been designed
with this use·case in mind.

Because of the nature of character sets, character processing, and text
rendering, this code is somewhat obtuse and I don’t recommend trying to
work with it directly unless you really need a lowlevel solution for
one of those things. Other, more accessible tools will be built on top
of this one (at some point…), and your time would be better spent
digging into them instead.

[Kixt Charsets]: <http://spec.go.kibi.family/-/kixt-charset/>
