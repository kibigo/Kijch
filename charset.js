//deno-lint-ignore-file no-cond-assign no-control-regex
// 🔤🟠 Kijch ∷ charset.js
// ====================================================================
//
// Copyright © 2019, 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { i18nNamespace, kixtNamespace } from "./names.js";

/** Enables constructors */
const okToConstruct = Symbol();

/** Internal constructor. */
function construct(Type, args, NewTarget) {
  const target = function () {};
  target.prototype = NewTarget?.prototype ?? Type.prototype;
  target.constructStatus = okToConstruct;
  return Reflect.construct(Type, args, target);
}

/** Defaults for enumerable properties. */
const visible = {
  configurable: false,
  enumerable: true,
};

/**
 * A collection of characters in a charset.
 *
 * This type can’t be constructed.
 */
export class CharacterCollection {
  #context;
  #name;

  constructor(context, name) {
    if (new.target.constructStatus !== okToConstruct) {
      throw new TypeError("Invalid constructor.");
    } else {
      this.#context = context;
      this.#name = String(name ?? "");
    }
  }

  /**
   * An iterator over codepoint∶Character pairs in the collection.
   */
  *entries() {
    for (const character of Object.values(this.#context.characters)) {
      if (character[this.#name] == this) {
        yield [character.codepoint, character];
      }
    }
  }

  /**
   * Returns the Character in the collection at the provided codepoint
   * or with the provided name, if one exists.
   */
  item(codepointOrName) {
    const n = +codepointOrName;
    if (
      n >= 0 && n <= -1 >>> 0 && Number.isInteger(n) &&
      String(n) === String(codepointOrName)
    ) {
      const character = this.#context.characters[n];
      if (character != null && character[this.#name] === this) {
        return character;
      } else {
        return undefined;
      }
    } else {
      const character = this.#context.namedCharacters[
        String(codepointOrName).toUpperCase().replace(/_/gu, " ")
      ];
      if (character != null && character[this.#name] === this) {
        return character;
      } else {
        return undefined;
      }
    }
  }

  /**
   * An iterator over codepoints in the collection.
   */
  *keys() {
    for (const character of Object.values(this.#context.characters)) {
      if (character[this.#name] == this) {
        yield character.codepoint;
      }
    }
  }

  /** An iterator over characters in the collection. */
  *values() {
    for (const character of Object.values(this.#context.characters)) {
      if (character[this.#name] == this) {
        yield character;
      }
    }
  }

  /**
   * An iterator over characters in the collection.
   *
   * Consider using `new Map(collection.entries())` instead of
   * `Array.from(collection)` if you want random codepoint access.
   */
  *[Symbol.iterator]() {
    for (const character of Object.values(this.#context.characters)) {
      if (character[this.#name] == this) {
        yield character;
      }
    }
  }
}

/**
 * A block in a charset.
 *
 * This type can’t be constructed.
 */
export class Block extends CharacterCollection {
  constructor(context, props) {
    if (new.target.constructStatus !== okToConstruct) {
      throw new TypeError("Invalid constructor.");
    } else {
      super(context, "block");
      const { aliases, alsoKnownAs, charset, name, notes } = props;
      Object.defineProperties(this, {
        aliases: {
          ...visible,
          get: () => new Set(aliases),
          set: undefined,
        },
        alsoKnownAs: {
          ...visible,
          get: () => new Set(alsoKnownAs),
          set: undefined,
        },
        charset: { ...visible, value: charset, writable: false },
        name: { ...visible, value: name, writable: false },
        notes: {
          ...visible,
          get: () => Array.from(notes),
          set: undefined,
        },
      });
    }
  }

  /** Returns a BlockDeclaration for the block. */
  toSource() {
    const { aliases, alsoKnownAs, notes } = this;
    return [
      `% ${this.name}`,
      ...function* () {
        for (const alias of aliases) {
          yield `= ${alias}`;
        }
        for (const a·k·a of alsoKnownAs) {
          yield `- ${a·k·a}`;
        }
        for (const note of notes) {
          yield `* ${note}`;
        }
      }(),
    ].join("\n");
  }
}

/**
 * A script in a charset.
 *
 * This type can’t be constructed.
 */
export class Script extends CharacterCollection {
  constructor(context, props) {
    if (new.target.constructStatus !== okToConstruct) {
      throw new TypeError("Invalid constructor.");
    } else {
      super(context, "script");
      const { alsoKnownAs, charset, iri, notes } = props;
      Object.defineProperties(this, {
        alsoKnownAs: {
          ...visible,
          get: () => new Set(alsoKnownAs),
          set: undefined,
        },
        charset: { ...visible, value: charset, writable: false },
        iri: { ...visible, value: iri, writable: false },
        notes: {
          ...visible,
          get: () => Array.from(notes),
          set: undefined,
        },
      });
    }
  }

  /** Returns a ScriptDeclaration for the script. */
  toSource() {
    const { alsoKnownAs, notes } = this;
    return [
      `' <${this.iri}>`,
      ...function* () {
        for (const a·k·a of alsoKnownAs) {
          yield `- ${a·k·a}`;
        }
        for (const note of notes) {
          yield `* ${note}`;
        }
      }(),
    ].join("\n");
  }
}

/**
 * A single character in a charset.
 *
 * This type can’t be constructed.
 */
export class Character {
  constructor(context, props) {
    if (new.target.constructStatus !== okToConstruct) {
      throw new TypeError("Invalid constructor.");
    } else {
      const {
        aliases,
        alsoKnownAs,
        basicType,
        block,
        charset,
        codepoint,
        combiningClass,
        compatibility,
        compatibilityMode,
        conjoiningClass,
        conjoiningMode,
        decomposition,
        decompositionPreferred,
        deprecated,
        fullwidth,
        extends: extends_,
        glyphs,
        linebreakAfter,
        linebreakBefore,
        linebreakCategory,
        name,
        notes,
        references,
        script,
        unicode,
      } = props;
      Object.defineProperties(this, {
        alsoKnownAs: {
          ...visible,
          get: () => new Set(alsoKnownAs),
          set: undefined,
        },
        aliases: {
          ...visible,
          get: () => new Set(aliases),
          set: undefined,
        },
        basicType: { ...visible, value: basicType, writable: false },
        block: { ...visible, value: block, writable: false },
        charset: { ...visible, value: charset, writable: false },
        codepoint: { ...visible, value: codepoint, writable: false },
        combiningClass: {
          ...visible,
          value: combiningClass,
          writable: false,
        },
        compatibility: {
          ...visible,
          get: () => compatibility.map(($) => context.characters[$]),
          set: undefined,
        },
        compatibilityMode: {
          ...visible,
          value: compatibilityMode,
          writable: false,
        },
        conjoiningClass: {
          ...visible,
          value: conjoiningClass,
          writable: false,
        },
        conjoiningMode: {
          ...visible,
          value: conjoiningMode,
          writable: false,
        },
        decomposition: {
          ...visible,
          get: () => decomposition.map(($) => context.characters[$]),
          set: undefined,
        },
        decompositionPreferred: {
          ...visible,
          value: decompositionPreferred,
          writable: false,
        },
        deprecated: { ...visible, value: deprecated, writable: false },
        extends: { ...visible, value: extends_, writable: false },
        fullwidth: { ...visible, value: fullwidth, writable: false },
        glyphs: { ...visible, value: glyphs, writable: false },
        linebreakAfter: {
          ...visible,
          value: linebreakAfter,
          writable: false,
        },
        linebreakBefore: {
          ...visible,
          value: linebreakBefore,
          writable: false,
        },
        linebreakCategory: {
          ...visible,
          value: linebreakCategory,
          writable: false,
        },
        name: { ...visible, value: name, writable: false },
        notes: {
          ...visible,
          get: () => Array.from(notes),
          set: undefined,
        },
        references: {
          ...visible,
          get: () =>
            Array.from(references).reduce(
              (result, $) => result.add(context.characters[$]),
              new Set(),
            ),
          set: undefined,
        },
        script: { ...visible, value: script, writable: false },
        unicode: {
          ...visible,
          get: () => Array.from(unicode),
          set: undefined,
        },
      });
    }
  }

  /**
   * Returns whether this character matches the provided character for
   * all compatibility properties.
   */
  compatibleWith(character) {
    const thisUnicode = Array.from(this.unicode);
    const otherUnicode = Array.from(character.unicode);
    if (
      this.basicType != character.basicType ||
      this.extends != character.extends ||
      this.combiningClass != character.combiningClass ||
      this.conjoiningMode != character.conjoiningMode ||
      this.conjoiningClass != character.conjoiningClass ||
      thisUnicode.length != otherUnicode.length
    ) {
      return false;
    } else {
      for (const [index, unicode] of thisUnicode.entries()) {
        if (unicode != otherUnicode[index]) {
          return false;
        } else {
          continue;
        }
      }
      return true;
    }
  }

  /**
   * Returns the image data for the largest glyph which fits within
   * the provided height.
   *
   * The second argument can be used to specify the colour of the glyph
   * and whether fullwidth glyphs should be preferred.
   */
  toImageData(size = Infinity, options) {
    const { glyphs } = this;
    const desiredHeight = +size;
    const fullwidth = options?.fullwidth ?? true;
    const color = (options?.color ?? 0xFF) >>> 0;
    const [width, height] = Object.keys(glyphs).reduce(
      ([bestWidth, bestHeight], key) => {
        const [width, height] = key.split("×").map(($) => parseInt($));
        if (!(`${width}×${height}` in glyphs)) {
          return [bestWidth, bestHeight];
        } else if (bestHeight == height) {
          return fullwidth && width == height ||
              !fullwidth && bestWidth == bestHeight
            ? [width, height]
            : [bestWidth, bestHeight];
        } else if (bestHeight <= desiredHeight) {
          return height > bestHeight && height <= desiredHeight
            ? [width, height]
            : [bestWidth, bestHeight];
        } else {
          return height < bestHeight
            ? [width, height]
            : [bestWidth, bestHeight];
        }
      },
      [Infinity, Infinity],
    );
    return !isFinite(height) ? null : new ImageData(
      Uint8ClampedArray.from(
        Array
          .from(glyphs[`${width}×${height}`])
          .flatMap((hex) => {
            const n = parseInt(hex, 16);
            return [n & 8, n & 4, n & 2, n & 1];
          })
          .flatMap((bit) =>
            !bit ? [0x00, 0x00, 0x00, 0x00] : [
              (color & 0xFF000000) >>> 24,
              (color & 0xFF0000) >>> 16,
              (color & 0xFF00) >>> 8,
              color & 0xFF,
            ]
          ),
      ),
      width,
      height,
    );
  }

  /** Returns a CharacterDeclaration for the character. */
  toSource() {
    const {
      aliases,
      alsoKnownAs,
      basicType,
      codepoint,
      combiningClass,
      compatibility,
      compatibilityMode,
      conjoiningClass,
      conjoiningMode,
      decomposition,
      decompositionPreferred,
      deprecated,
      extends: extends_,
      fullwidth,
      glyphs,
      linebreakAfter,
      linebreakBefore,
      linebreakCategory,
      notes,
      references,
    } = this;
    const character = this;
    return [
      ...this.unicode.map((codepoint) =>
        `U+${
          codepoint > 0xFFFF
            ? codepoint.toString(16).toUpperCase()
            : new Array(
              3 - (Math.log2(codepoint) / 4 >>> 0),
            ).fill(0).join("") + codepoint.toString(16).toUpperCase()
        }`
      ),
      `; ${
        new Array(
          3 - (Math.log2(codepoint) / 4 >>> 0),
        ).fill(0).join("") + codepoint.toString(16).toUpperCase()
      } ${this.name} (${
        basicType.startsWith(kixtNamespace)
          ? basicType.substring(kixtNamespace.length)
          : basicType
      })`,
      ...function* () {
        if (
          basicType == `${kixtNamespace}SPACING` && linebreakCategory
        ) {
          yield `: ${linebreakCategory} (${linebreakBefore} ${linebreakAfter})`;
        }
        if (
          compatibility.length > 1 || compatibility[0] !== character
        ) {
          yield `( ${
            compatibilityMode != `${kixtNamespace}GENERIC`
              ? `<${compatibilityMode}> `
              : ""
          }${
            compatibility.map(({ codepoint }) =>
              new Array(
                3 - (Math.log2(codepoint) / 4 >>> 0),
              ).fill(0).join("") + codepoint.toString(16).toUpperCase()
            ).join(" ")
          }`;
        }
        if (
          decomposition.length > 1 || decomposition[0] !== character
        ) {
          yield `${decompositionPreferred ? ">>" : ">"} ${
            decomposition.map(({ codepoint }) =>
              new Array(
                3 - (Math.log2(codepoint) / 4 >>> 0),
              ).fill(0).join("") + codepoint.toString(16).toUpperCase()
            ).join(" ")
          }`;
        }
        if (
          deprecated ||
          (basicType == `${kixtNamespace}SPACING` ||
              basicType == `${kixtNamespace}NONSPACING`) &&
            (fullwidth != null || extends_ || conjoiningMode != null)
        ) {
          yield [
            "&",
            ...function* () {
              if (deprecated) {
                yield "DEPRECATED";
              }
              if (fullwidth != null) {
                yield fullwidth ? "FULLWIDTH" : "PROPORTIONAL";
              }
              if (conjoiningMode != null) {
                yield `CONJOINS<${conjoiningMode}>${
                  conjoiningClass || ""
                }`;
              }
              if (extends_) {
                yield `EXTENDS+${combiningClass}`;
              }
            }(),
          ].join(" ");
        }
        for (const alias of aliases) {
          yield `= ${alias}`;
        }
        for (const a·k·a of alsoKnownAs) {
          yield `- ${a·k·a}`;
        }
        for (const note of notes) {
          yield `* ${note}`;
        }
        for (const { codepoint } of references) {
          yield `> ${
            new Array(
              3 - (Math.log2(codepoint) / 4 >>> 0),
            ).fill(0).join("") + codepoint.toString(16).toUpperCase()
          }`;
        }
        for (const glyph of Object.values(glyphs)) {
          yield `) ${glyph}`;
        }
      }(),
    ].join("\n");
  }

  /** Returns the character as a Unicode string. */
  toString() {
    return String.fromCodePoint(...this.unicode);
  }

  /** Returns the codepoint of the character. */
  valueOf() {
    const result = +this.codepoint;
    if (result !== Math.min(Math.max(result | 0, 0), 0xFFFF)) {
      throw new RangeError(`Kijch: Codepoint out of range: ${result}`);
    } else {
      return result;
    }
  }
}

/**
 * A Kixt charset.
 */
export default class Charset extends CharacterCollection {
  /**
   * Makes a new `Charset` from the provided `src` string.
   */
  constructor(src) {
    const source = `${src}`;
    if (!/[\x0A\x0D\x85\u2028]/u.test(source[source.length - 1])) {
      throw new TypeError("Document does not end in a break.");
    } else {
      // This code is not pretty but it does what it needs to.

      const context = Object.create(null);
      const result = construct(
        CharacterCollection,
        [context, "charset"],
        Charset,
      );

      const blocks = context.blocks = Object.create(null);
      const characters = context.characters = Object.create(null);
      const namedCharacters = Object.create(null);
      const scripts = context.scripts = [
        `${i18nNamespace}zyyy`,
        `${i18nNamespace}zinh`,
        `${i18nNamespace}zzzz`,
      ].reduce((scripts, key) => {
        scripts[key] = construct(Script, [context, {
          charset: result,
          iri: key,
        }]);
        return scripts;
      }, Object.create(null));

      let index = 0;
      const lines = source.split(
        /[\x0A\x85\u2028]|\x0D\x0A|\x0D\x85|\x0D(?![\x0A\x85])/gu,
      );
      let line = lines[index];
      let match = null;
      let inMultiLineComment = false;
      let currentBlock = null;
      let currentScript = scripts[`${i18nNamespace}zzzz`];

      let iri = undefined;
      let version = undefined;
      let revision = undefined;
      let supportsVariableEncoding = false;
      const alsoKnownAs = new Set();
      const notes = [];

      const usedBlocks = new Set();
      const usedCodepoints = new Set();
      const usedNames = new Set();

      const moveToNextNonCommentLine = () => {
        do line = lines[++index]; while (
          /^ *\/[^\x00-\x1F\x7F-\x9F\uD800-\uDFFF\uFDD0-\uFDEF\uFFF0-\uFFFF\u{1FFFE}\u{1FFFF}\u{2FFFE}\u{2FFFF}\u{3FFFE}\u{3FFFF}\u{4FFFE}\u{4FFFF}\u{5FFFE}\u{5FFFF}\u{6FFFE}\u{6FFFF}\u{7FFFE}\u{7FFFF}\u{8FFFE}\u{8FFFF}\u{9FFFE}\u{9FFFF}\u{AFFFE}\u{AFFFF}\u{BFFFE}\u{BFFFF}\u{CFFFE}\u{CFFFF}\u{DFFFE}-\u{E0FFF}\u{EFFFE}\u{EFFFF}\u{FFFFE}\u{FFFFF}\u{10FFFE}\u{10FFFF}]*$/u
            .test(line)
        );
      };
      const collectAliasesInto = (collection, used) => {
        while (
          match = line.match(
            /^ *= *([A-Z](?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])|-(?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])))*) *$/u,
          )
        ) { // <Aliases>
          collection.add(match[1]);
          if (used.has(match[1])) {
            throw new TypeError(
              `Redefinition of name ${match[1]} on line ${
                index + 1
              } of input.`,
            );
          } else used.add(match[1]);
          moveToNextNonCommentLine();
        }
      };
      const collectAlsoKnownAsInto = (collection) => {
        while (
          match = line.match(
            /^ *- *([\x21-\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}](?: ?[\x21-\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}])*) *$/u,
          )
        ) { // <OtherNames>
          collection.add(match[1]);
          moveToNextNonCommentLine();
        }
      };
      const collectNotesInto = (collection) => {
        while (
          match = line.match(
            /^ *\* *([\x21-\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}](?: ?[\x21-\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}])*) *$/u,
          )
        ) { // <Notes>
          collection.push(match[1]);
          moveToNextNonCommentLine();
        }
      };

      while (index < lines.length) {
        if (index == 0) { // <CharsetDeclaration>
          if (
            !(match = line.match(
              /^(?:\uFEFF)?;CHARSET<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)>(?:(0|[1-9A-F][0-9A-F]{0,3})(?:\.(0|[1-9A-F][0-9A-F]{0,3}))?)? *$/u,
            ))
          ) {
            throw new TypeError(
              "Document does not begin with a charset declaration.",
            ); // <CharsetIdentifier>
          }
          try {
            new URL(iri = match[1]);
          } catch {
            throw new TypeError(
              "Document does not begin with a charset declaration.",
            );
          }
          if (match[2]) version = parseInt(match[2], 16);
          if (match[3]) revision = parseInt(match[3], 16);
          moveToNextNonCommentLine();
          if (match = line.match(/^ *& *VARIABLE *$/u)) { // <CharsetProperties>
            supportsVariableEncoding = true;
            moveToNextNonCommentLine();
          }
          collectAlsoKnownAsInto(alsoKnownAs);
          collectNotesInto(notes);
          continue;
        }
        if (/^\.\.\.$/u.test(line)) { // <MultiLineComment> begins
          inMultiLineComment = true;
          line = lines[++index];
          continue;
        }
        if (inMultiLineComment) { // inside <MultiLineComment>
          if (/^\/\/\/$/u.test(line)) { // <MultiLineComment> ends
            inMultiLineComment = false;
            moveToNextNonCommentLine();
            continue;
          }
          if (
            !/^[^\x00-\x1F\x7F-\x9F\uD800-\uDFFF\uFDD0-\uFDEF\uFFF0-\uFFFF\u{1FFFE}\u{1FFFF}\u{2FFFE}\u{2FFFF}\u{3FFFE}\u{3FFFF}\u{4FFFE}\u{4FFFF}\u{5FFFE}\u{5FFFF}\u{6FFFE}\u{6FFFF}\u{7FFFE}\u{7FFFF}\u{8FFFE}\u{8FFFF}\u{9FFFE}\u{9FFFF}\u{AFFFE}\u{AFFFF}\u{BFFFE}\u{BFFFF}\u{CFFFE}\u{CFFFF}\u{DFFFE}-\u{E0FFF}\u{EFFFE}\u{EFFFF}\u{FFFFE}\u{FFFFF}\u{10FFFE}\u{10FFFF}]*$/u
              .test(line)
          ) {
            throw new TypeError(
              `Invalid character in multiline comment on line ${
                index + 1
              } of input.`,
            );
          }
          line = lines[++index];
          continue;
        }
        if (
          match = line.match(
            /^ *% *([A-Z](?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])|-(?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])))*) *$/u,
          )
        ) { // <BlockDeclaration>
          if (match[1] == "NO BLOCK") {
            currentBlock = null;
            moveToNextNonCommentLine();
            continue;
          }
          const name = match[1]; // <BlockName>
          if (usedBlocks.has(name)) {
            throw new TypeError(
              `Redefinition of block ${name} on line ${
                index + 1
              } of input.`,
            );
          }
          const props = Object.assign(Object.create(null), {
            aliases: new Set(),
            alsoKnownAs: new Set(),
            charset: result,
            name,
            notes: [],
          });
          usedBlocks.add(name);
          moveToNextNonCommentLine();
          collectAliasesInto(props.aliases, usedBlocks);
          collectAlsoKnownAsInto(props.alsoKnownAs);
          collectNotesInto(props.notes);
          currentBlock = blocks[name] = construct(Block, [
            context,
            props,
          ]);
          for (const alias of props.aliases) {
            blocks[alias] = currentBlock;
          }
          continue;
        }
        if (
          match = line.match(
            /^ *\x27 *<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)> *$/u,
          )
        ) { // <ScriptDeclaration>
          const iri = match[1]; // <ScriptIdentifier>
          const props = iri in scripts
            ? Object.assign(Object.create(null), scripts[iri])
            : Object.assign(Object.create(null), {
              alsoKnownAs: new Set(),
              charset: result,
              iri,
              notes: [],
            });
          try {
            new URL(iri);
          } catch {
            throw new TypeError(
              `The IRI in the script declaration is not well-formed on line ${
                index + 1
              } of input.`,
            );
          }
          moveToNextNonCommentLine();
          collectAlsoKnownAsInto(props.alsoKnownAs);
          collectNotesInto(props.notes);
          currentScript = scripts[iri] = construct(Script, [
            context,
            props,
          ]);
          continue;
        }
        if (
          /^ *U\+(?:0*(?:10|[1-9A-F])[0-9A-F]{0,4}|0+)(?: [^\x00-\x1F\x7F-\x9F\uD800-\uDFFF\uFDD0-\uFDEF\uFFF0-\uFFFF\u{1FFFE}\u{1FFFF}\u{2FFFE}\u{2FFFF}\u{3FFFE}\u{3FFFF}\u{4FFFE}\u{4FFFF}\u{5FFFE}\u{5FFFF}\u{6FFFE}\u{6FFFF}\u{7FFFE}\u{7FFFF}\u{8FFFE}\u{8FFFF}\u{9FFFE}\u{9FFFF}\u{AFFFE}\u{AFFFF}\u{BFFFE}\u{BFFFF}\u{CFFFE}\u{CFFFF}\u{DFFFE}-\u{E0FFF}\u{EFFFE}\u{EFFFF}\u{FFFFE}\u{FFFFF}\u{10FFFE}\u{10FFFF}]*)?$/u
            .test(line)
        ) { // <CharacterDefinition>
          const block = currentBlock;
          const script = currentScript;
          const references = new Set();
          const glyphs = Object.create(null);
          const props = Object.assign(Object.create(null), {
            aliases: new Set(),
            alsoKnownAs: new Set(),
            block,
            charset: result,
            codepoint: -1,
            combiningClass: 0,
            compatibility: null,
            compatibilityMode: `${kixtNamespace}GENERIC`,
            conjoiningClass: 0,
            conjoiningMode: undefined,
            decomposition: null,
            deprecated: undefined,
            extends: undefined,
            fullwidth: undefined,
            glyphs,
            linebreakAfter: undefined,
            linebreakBefore: undefined,
            linebreakCategory: undefined,
            name: null,
            notes: [],
            references,
            script,
            unicode: [],
          });
          while (
            match = line.match(
              /^ *U\+(0*(?:10|[1-9A-F])[0-9A-F]{0,4}|0+)(?: [^\x00-\x1F\x7F-\x9F\uD800-\uDFFF\uFDD0-\uFDEF\uFFF0-\uFFFF\u{1FFFE}\u{1FFFF}\u{2FFFE}\u{2FFFF}\u{3FFFE}\u{3FFFF}\u{4FFFE}\u{4FFFF}\u{5FFFE}\u{5FFFF}\u{6FFFE}\u{6FFFF}\u{7FFFE}\u{7FFFF}\u{8FFFE}\u{8FFFF}\u{9FFFE}\u{9FFFF}\u{AFFFE}\u{AFFFF}\u{BFFFE}\u{BFFFF}\u{CFFFE}\u{CFFFF}\u{DFFFE}-\u{E0FFF}\u{EFFFE}\u{EFFFF}\u{FFFFE}\u{FFFFF}\u{10FFFE}\u{10FFFF}]*)?$/u,
            )
          ) { // <UnicodeMapping>
            props.unicode.push(parseInt(match[1], 16));
            moveToNextNonCommentLine();
          }
          match = line.match(
            /^ *; *(?:((?:0 ?)*1(?: ?[01])*|(?:0 ?)+) *\/|(0*[1-9A-F][0-9A-F]{0,3}|0+) ) *([A-Z](?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])|-(?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])))*) *\((CONTROL|MESSAGING|FORMAT|DATA|NONSPACING|SPACING|PRIVATEUSE|NONCHARACTER)\) *$/u,
          ); // <CharacterInfo>
          if (!match) {
            throw new TypeError(
              `Character definition does not contain well-formed character info on line ${
                index + 1
              } of input.`,
            );
          }
          const codepoint = props.codepoint = match[1]
            ? parseInt(match[1].replace(/ /gu, ""), 2)
            : parseInt(match[2], 16);
          if (codepoint in characters) {
            throw new TypeError(
              `Redefinition of codepoint ${
                new Array(
                  3 - (Math.log2(codepoint) / 4 >>> 0),
                ).fill(0).join("") +
                codepoint.toString(16).toUpperCase()
              } on line ${index + 1} of input.`,
            );
          }
          props.compatibility = [codepoint];
          props.decomposition = [codepoint];
          const name = props.name = match[3];
          if (usedNames.has(name)) {
            throw new TypeError(
              `Redefinition of name ${name} on line ${
                index + 1
              } of input.`,
            );
          } else {
            usedNames.add(name);
          }
          const basicType = props.basicType = `${kixtNamespace}${
            match[4]
          }`;
          moveToNextNonCommentLine();
          if (
            match = line.match(
              /^ *: *([A-Z](?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])|-(?:[0-9A-Z]| (?:[A-Z]|-[0-9A-Z])))*)(?: *\((SEPARABLE|INSEPARABLE|BREAK|JOIN)(?: *(SEPARABLE|INSEPARABLE|BREAK|JOIN))?\))? *$/u,
            )
          ) { // <LinebreakProperties>
            if (basicType != `${kixtNamespace}SPACING`) {
              throw new TypeError(
                `A character has linebreak properties but is not SPACING on line ${
                  index + 1
                } of input.`,
              );
            }
            props.linebreakCategory = match[1];
            props.linebreakBefore = `${kixtNamespace}${
              match[2] || "INSEPARABLE"
            }`;
            props.linebreakAfter = `${kixtNamespace}${
              match[3] || match[2] || "INSEPARABLE"
            }`;
            moveToNextNonCommentLine();
          }
          if (
            match = line.match(
              /^ *\( *(?:<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)>)? *((?:0*[1-9A-F][0-9A-F]{0,3}|0+)(?: +(?:0*[1-9A-F][0-9A-F]{0,3}|0+))*) *$/u,
            )
          ) { // <CompatibilityMapping>
            const compatibility = props.compatibility = match[2].trim()
              .split(/ +/gu).map(
                (value) => parseInt(value, 16),
              );
            const compatibilityMode = props.compatibilityMode =
              match[1] || `${kixtNamespace}GENERIC`;
            if (compatibilityMode) {
              try {
                new URL(compatibilityMode);
              } catch {
                `The IRI in the compatibility mapping is not well-formed on line ${
                  index + 1
                } of input.`;
              }
            }
            if (
              compatibility.length == 1 &&
              compatibility[0] == codepoint &&
              compatibilityMode != `${kixtNamespace}GENERIC`
            ) {
              throw new TypeError(
                `A character has a compatibility mapping to itself but a non-GENERIC compatibility mode on line ${
                  index + 1
                } of input.`,
              );
            }
            for (const value of compatibility) {
              usedCodepoints.add(value);
            }
            moveToNextNonCommentLine();
          }
          if (
            match = line.match(
              /^ *(?:< *((?:0*[1-9A-F][0-9A-F]{0,3}|0+)(?: +(?:0*[1-9A-F][0-9A-F]{0,3}|0+))*)|<< *((?:0*[1-9A-F][0-9A-F]{0,3}|0+)(?: +(?:0*[1-9A-F][0-9A-F]{0,3}|0+))+)) *$/u,
            )
          ) { // <DecompositionMapping>
            const decomposition = props.decomposition =
              (match[1] || match[2]).trim().split(/ +/gu).map((
                value,
              ) => parseInt(value, 16));
            props.decompositionPreferred = decomposition.length == 1 ||
              !match[1];
            for (const value of decomposition) {
              usedCodepoints.add(value);
            }
            moveToNextNonCommentLine();
          }
          if (
            match = line.match(
              /^ *& *(?:(DEPRECATED)(?: +(?:(PROPORTIONAL|FULLWIDTH)(?: +CONJOINS<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)>)?|CONJOINS<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)>(?:\|(0|[1-9A-F][0-9A-F]{0,3}))?|(EXTENDS)(?:\+(0|[1-9A-F][0-9A-F]{0,3}))?))?|(PROPORTIONAL|FULLWIDTH)(?: +CONJOINS<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)>)?|CONJOINS<([A-Za-z][0-9A-Za-z+\.-]*:(?:[0-9A-Za-z\x21\x23\x24\x26-\x2F\x3A\x3B\x3D\x3F\x40\x5B\x5D\x5F\x7E\xA0-\uD7FF\uE000-\uFDCF\uFDF0-\uFFEF\u{10000}-\u{1FFFD}\u{20000}-\u{2FFFD}\u{30000}-\u{3FFFD}\u{40000}-\u{4FFFD}\u{50000}-\u{5FFFD}\u{60000}-\u{6FFFD}\u{70000}-\u{7FFFD}\u{80000}-\u{8FFFD}\u{90000}-\u{9FFFD}\u{A0000}-\u{AFFFD}\u{B0000}-\u{BFFFD}\u{C0000}-\u{CFFFD}\u{D0000}-\u{DFFFD}\u{E1000}-\u{EFFFD}\u{F0000}-\u{FFFFD}\u{100000}-\u{10FFFD}]|%[0-9A-Za-z]{2})*)>(?:\|(0|[1-9A-F][0-9A-F]{0,3}))?|(EXTENDS)(?:\+(0|[1-9A-F][0-9A-F]{0,3}))?) *$/u,
            )
          ) { // <AdditionalProperties>
            props.deprecated = !!match[1];
            const extends_ = props.extends = !!(match[6] || match[12]);
            const fullwidth = props.fullwidth = match[2] || match[8]
              ? (match[2] || match[8]) == "FULLWIDTH"
              : null;
            if (
              !extends_ && script.iri == `${kixtNamespace}INHERITED`
            ) {
              throw new TypeError(
                `The INHERITED script was declared for a non-combining character on line ${
                  index + 1
                } of input.`,
              );
            }
            props.combiningClass =
              parseInt(match[7] || match[13], 16) >>> 0;
            const conjoiningMode = props.conjoiningMode = match[3] ||
              match[4] || match[9] ||
              match[10] || null;
            if (conjoiningMode) {
              try {
                new URL(conjoiningMode);
              } catch {
                throw new TypeError(
                  `The IRI in the additional properties is not well-formed on line ${
                    index + 1
                  } of input.`,
                );
              }
            }
            if (
              basicType != `${kixtNamespace}SPACING` &&
              basicType != `${kixtNamespace}NONSPACING` &&
              (fullwidth != null || extends_ != null ||
                conjoiningMode != null)
            ) {
              throw new TypeError(
                `An additional property other than DEPRECATED was provided for a non-graphic (spacing or nonspacing) character on line ${
                  index + 1
                } of input.`,
              );
            }
            props.conjoiningClass =
              parseInt(match[5] || match[11], 16) >>> 0;
            moveToNextNonCommentLine();
          }
          collectAliasesInto(props.aliases, usedNames);
          collectAlsoKnownAsInto(props.alsoKnownAs);
          collectNotesInto(props.notes);
          while (
            match = line.match(
              /^ *> *(0*[1-9A-F][0-9A-F]{0,3}|0+)(?: [^\x00-\x1F\x7F-\x9F\uD800-\uDFFF\uFDD0-\uFDEF\uFFF0-\uFFFF\u{1FFFE}\u{1FFFF}\u{2FFFE}\u{2FFFF}\u{3FFFE}\u{3FFFF}\u{4FFFE}\u{4FFFF}\u{5FFFE}\u{5FFFF}\u{6FFFE}\u{6FFFF}\u{7FFFE}\u{7FFFF}\u{8FFFE}\u{8FFFF}\u{9FFFE}\u{9FFFF}\u{AFFFE}\u{AFFFF}\u{BFFFE}\u{BFFFF}\u{CFFFE}\u{CFFFF}\u{DFFFE}-\u{E0FFF}\u{EFFFE}\u{EFFFF}\u{FFFFE}\u{FFFFF}\u{10FFFE}\u{10FFFF}]*)?$/u,
            )
          ) { // <References>
            references.add(parseInt(match[1], 16));
            moveToNextNonCommentLine();
          }
          for (const value of references) {
            usedCodepoints.add(value);
          }
          while (
            match = line.match(/^ *\) *([0-9A-F]{8,}) *$/u)
          ) { // <Glyphs>
            let hex = match[1];
            let sqrt = 0;
            let { length } = hex;
            let size;
            while (
              !(
                (
                            sqrt = Math.floor(Math.sqrt(length * 4))
                          ) * sqrt == length * 4 && (
                      size = `${sqrt}×${sqrt}`
                    ) || (
                              sqrt = Math.floor(Math.sqrt(length * 8))
                            ) * sqrt == length * 8 && (
                      size = `${sqrt / 2}×${sqrt}`
                    )
              ) || length % 2
            ) {
              hex += "0";
              ++length;
            }
            if (size in glyphs) {
              throw new TypeError(
                `Multiple glyphs of the same length are defined for a character on line ${
                  index + 1
                } of input.`,
              );
            }
            glyphs[size] = hex;
            moveToNextNonCommentLine();
          }
          const character = characters[codepoint] = construct(
            Character,
            [context, props],
          );
          namedCharacters[name] = characters[codepoint];
          for (const alias of props.aliases) {
            namedCharacters[alias] = character;
          }
          continue;
        }
        if (/^ *$/u.test(line)) { // <Blank>
          moveToNextNonCommentLine();
          continue;
        }
        throw new TypeError(
          `Unrecognized syntax on line ${index + 1} of input.`,
        );
      }

      for (const codepoint of usedCodepoints) {
        if (!(codepoint in characters)) {
          throw new TypeError(
            `A reference was made to codepoint ${
              new Array(
                3 - (Math.log2(codepoint) / 4 >>> 0),
              ).fill(0).join("") + codepoint.toString(16).toUpperCase()
            }, but it was never defined.`,
          );
        }
      }

      context.blocks = Object.freeze(blocks);
      context.scripts = Object.freeze(scripts);
      context.characters = Object.freeze(characters);
      context.namedCharacters = Object.freeze(namedCharacters);
      Object.freeze(context);

      Object.defineProperties(result, {
        alsoKnownAs: {
          ...visible,
          get: () => new Set(alsoKnownAs),
          set: undefined,
        },
        iri: { ...visible, value: iri, writable: false },
        notes: {
          ...visible,
          get: () => Array.from(notes),
          set: undefined,
        },
        revision: { ...visible, value: revision, writable: false },
        supportsVariableEncoding: {
          ...visible,
          value: supportsVariableEncoding,
          writable: false,
        },
        version: { ...visible, value: version, writable: false },
      });
      return result;
    }
  }

  toSource() {
    const { alsoKnownAs, notes, revision, version } = this;
    const characters = this.values();
    return `${
      [
        `;CHARSET<${this.iri}>${
          [
            Array.from(function* () {
              if (version != null) {
                yield version;
              }
              if (revision != null) {
                yield revision;
              }
            }()).join("."),
            ...function* () {
              for (const a·k·a of alsoKnownAs) {
                yield `- ${a·k·a}`;
              }
              for (const note of notes) {
                yield `* ${note}`;
              }
            }(),
          ].join("\n")
        }`,
        ...function* () {
          let currentBlock = null;
          let currentScript = `${i18nNamespace}zzzz`;
          for (const character of characters) {
            const { block, script } = character;
            const { iri: scriptIRI } = script;
            if (block != currentBlock) {
              yield block == null
                ? "% NO BLOCK"
                : Block.prototype.toSource.call(block);
              currentBlock = block;
            }
            if (scriptIRI != currentScript) {
              yield Script.prototype.toSource.call(script);
              currentScript = scriptIRI;
            }
            yield Character.prototype.toSource.call(character);
          }
        },
      ].join("\n\n")
    }
`;
  }
}
