// 🔤🟠 Kijch ∷ names.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

/** The Kixt namespace. */
export const kixtNamespace = "https://ns.1024.gdn/Kixt/#";

/** The I·18·N namespace. */
export const i18nNamespace = "https://www.w3.org/ns/i18n#";

/** The X·H·T·M·L namespace. */
export const xhtmlNamespace = "http://www.w3.org/1999/xhtml";
