// 🔤🟠 Kijch ∷ requiredCharacters.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { Character } from "./charset.js";
import { kixtNamespace } from "./names.js";

const defaultAdditionalProperties = {
  extends: undefined,
  combiningClass: 0,
  conjoiningMode: undefined,
  conjoiningClass: 0,
};

/**
 * Characters required to be in a charset in order for it to be used
 * to construct text.
 *
 * Only compatibility properties must match. Some properties, like
 * those for linebreaking, are not valid in actual charset definitions
 * but are useful for this program. In particular :—
 *
 * - The presence of `fullwidth` on a formatting character indicates
 *   that the character is meant to take up space (this is primarily
 *   true for spaces). The value may be `undefined`.
 *
 * - A value of `true` for `linebreakAfter` indicates that a character
 *   produces a hard break.
 *
 * - `isTransmissionCharacter` will be `true` for transmission
 *   characters.
 *
 * - `ignorable` will be `true` on formatting characters which are not
 *   meant to output a rendered glyph.
 */
const requiredCharacters = Object.create(
  Object.create(null, {
    toString: {
      configurable: true,
      enumerable: false,
      value: function toString() {
        return [
          ";CHARSET<https://kibigo.gitlab.io/Kijch/#requiredCharacters>",
          ...function* () {
            for (
              const character of Object.values(requiredCharacters)
            ) {
              yield Character.prototype.toSource.call(character);
            }
          }(),
        ].join("\n\n");
      },
      writable: true,
    },
    [Symbol.iterator]: {
      configurable: false,
      enumerable: false,
      value: function* () {
        for (const character of Object.values(this)) {
          yield character;
        }
      },
      writable: false,
    },
  }),
);
for (
  const [codepoint, value] of [
    [0x00, {
      name: "NULL",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x00],
      ignorable: true,
    }],
    [0x01, {
      name: "HEAD",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x01],
      isTransmissionCharacter: true,
    }],
    [0x02, {
      name: "BEGIN",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x02],
      isTransmissionCharacter: true,
    }],
    [0x03, {
      name: "FINISH",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x03],
      isTransmissionCharacter: true,
    }],
    [0x04, {
      name: "DONE",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x04],
      isTransmissionCharacter: true,
    }],
    [0x05, {
      name: "ENQUIRY",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x05],
    }],
    [0x06, {
      name: "CONFIRM",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x06],
    }],
    [0x07, {
      name: "ALERT",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x07],
    }],
    [0x08, {
      name: "BACK",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x08],
    }],
    [0x09, {
      name: "COLUMN NEXT",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x09],
    }],
    [0x0A, {
      name: "ADVANCE",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x0A],
    }],
    [0x0B, {
      name: "ROW NEXT",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x0B],
    }],
    [0x0C, {
      name: "PAGE END",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x0C],
    }],
    [0x0D, {
      name: "LINE START",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x0D],
    }],
    [0x0E, {
      name: "LEAVE",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x0E],
    }],
    [0x0F, {
      name: "RETURN",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x0F],
    }],
    [0x10, {
      name: "DATA",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x10],
    }],
    [0x11, {
      name: "BOOT",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x11],
    }],
    [0x12, {
      name: "RESUME",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x12],
    }],
    [0x13, {
      name: "SUSPEND",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x13],
    }],
    [0x14, {
      name: "HALT",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x14],
    }],
    [0x15, {
      name: "ERROR",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x15],
    }],
    [0x16, {
      name: "IDLE",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x16],
      isTransmissionCharacter: true,
    }],
    [0x17, {
      name: "BREAK",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x17],
      isTransmissionCharacter: true,
    }],
    [0x18, {
      name: "CANCEL",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x18],
      isTransmissionCharacter: true,
    }],
    [0x19, {
      name: "END",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x19],
      isTransmissionCharacter: true,
    }],
    [0x1A, {
      name: "INVALID",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x1A],
      isTransmissionCharacter: true,
    }],
    [0x1B, {
      name: "COMMAND",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x1B],
    }],
    [0x1C, {
      name: "VOLUME SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x1C],
    }],
    [0x1D, {
      name: "PART SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x1D],
    }],
    [0x1E, {
      name: "CHAPTER SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x1E],
    }],
    [0x1F, {
      name: "SECTION SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x1F],
    }],
    [0x20, {
      name: "JUSTIFIABLE QUAD",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      fullwidth: undefined,
      linebreakCategory: "'SPACE",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}BREAK`,
      unicode: [0x20],
    }],
    [0x7F, {
      name: "NOTHING",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x7F],
    }],
    ...function* () {
      for (let index = 0; index < 0x20; ++index) {
        yield [0x80A0 + index, {
          name: `NONCHARACTER-${index + 1}`,
          basicType: `${kixtNamespace}NONCHARACTER`,
          ...defaultAdditionalProperties,
          unicode: [0xFDD0 + index],
        }];
      }
    }(),
    [0x80C0, {
      name: "SELECTED BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x86],
      ignorable: true,
    }],
    [0x80C1, {
      name: "SELECTED FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x87],
      ignorable: true,
    }],
    [0x80C2, {
      name: "PROTECTED BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x96],
      ignorable: true,
    }],
    [0x80C3, {
      name: "PROTECTED FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x97],
      ignorable: true,
    }],
    [0x80C4, {
      name: "ANNOTATED BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xFFF9],
      ignorable: true,
    }],
    [0x80C5, {
      name: "ANNOTATION",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xFFFA],
      ignorable: true,
    }],
    [0x80C6, {
      name: "ANNOTATED FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xFFFB],
      ignorable: true,
    }],
    [0x80C7, {
      name: "STRING BEGIN",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x98],
    }],
    [0x80C8, {
      name: "DONT",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x9A],
    }],
    [0x80C9, {
      name: "STRING FINISH",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x9C],
    }],
    [0x80CA, {
      name: "APPLICATION COMMAND",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x9F],
    }],
    [0x80CB, {
      name: "USER COMMAND",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x9E],
    }],
    [0x80CC, {
      name: "OPERATING SYSTEM COMMAND",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x9D],
    }],
    [0x80CD, {
      name: "DEVICE COMMAND",
      basicType: `${kixtNamespace}MESSAGING`,
      ...defaultAdditionalProperties,
      unicode: [0x90],
    }],
    [0x80CE, {
      name: "OBJECT",
      basicType: `${kixtNamespace}SPACING`,
      ...defaultAdditionalProperties,
      unicode: [0xFFFC],
    }],
    [0x80CF, {
      name: "REPLACEMENT",
      basicType: `${kixtNamespace}SPACING`,
      ...defaultAdditionalProperties,
      unicode: [0xFFFD],
    }],
    [0x80D0, {
      name: "ROW SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x85],
    }],
    [0x80D1, {
      name: "COLUMN SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x89],
    }],
    [0x80D2, {
      name: "PARAGRAPH SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x2029],
    }],
    [0x80D3, {
      name: "LINE SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: true,
      unicode: [0x2028],
    }],
    [0x80D4, {
      name: "WORD SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}BREAK`,
      linebreakAfter: `${kixtNamespace}BREAK`,
      unicode: [0x200B],
    }],
    [0x80D5, {
      name: "WORD JOINER",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'BREAK",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}JOIN`,
      unicode: [0x2060],
    }],
    [0x80D6, {
      name: "INTERRUPT",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x95],
    }],
    [0x80D7, {
      name: "SHIFT",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x8E],
    }],
    [0x80D8, {
      name: "FORWARD",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x81],
    }],
    [0x80D9, {
      name: "COLUMN PREVIOUS",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x88],
    }],
    [0x80DA, {
      name: "RETRACT",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x8D],
    }],
    [0x80DB, {
      name: "ROW PREVIOUS",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x8A],
    }],
    [0x80DC, {
      name: "PAGE START",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x93],
    }],
    [0x80DD, {
      name: "LINE END",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x84],
    }],
    [0x80DE, {
      name: "IGNORE",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x9B],
    }],
    [0x80DF, {
      name: "OOPS",
      basicType: `${kixtNamespace}CONTROL`,
      ...defaultAdditionalProperties,
      unicode: [0x94],
    }],
    [0x81A0, {
      name: "PHRASE BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x93],
      ignorable: true,
    }],
    [0x81A1, {
      name: "PHRASE FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x94],
      ignorable: true,
    }],
    [0x81A2, {
      name: "NONSORTING BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x88],
      ignorable: true,
    }],
    [0x81A3, {
      name: "NONSORTING FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x89],
      ignorable: true,
    }],
    [0x81A4, {
      name: "PRIMARY SORTPHRASE BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x95],
      ignorable: true,
    }],
    [0x81A5, {
      name: "SORTPHRASE WORD JOINER",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x87],
      ignorable: true,
    }],
    [0x81A6, {
      name: "PRIMARY SORTPHRASE FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x96],
      ignorable: true,
    }],
    [0x81A7, {
      name: "KEYPHRASE BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x9C],
      ignorable: true,
    }],
    [0x81A8, {
      name: "IDENTIFIER",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x8C],
      ignorable: true,
    }],
    [0x81A9, {
      name: "KEYPHRASE FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x9D],
      ignorable: true,
    }],
    [0x81AA, {
      name: "SECONDARY SORTPHRASE BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x97],
      ignorable: true,
    }],
    [0x81AB, {
      name: "SECONDARY SORTPHRASE FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x98],
      ignorable: true,
    }],
    [0x81AC, {
      name: "HEADPHRASE BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x9E],
      ignorable: true,
    }],
    [0x81AD, {
      name: "HEADPHRASE FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x9F],
      ignorable: true,
    }],
    [0x81AE, {
      name: "COMMENT BEGIN",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x91],
      ignorable: true,
    }],
    [0x81AF, {
      name: "COMMENT FINISH",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x92],
      ignorable: true,
    }],
    [0x81B0, {
      name: "LIGATION SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'SPACE",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}JOIN`,
      unicode: [0x200C],
      ignorable: true,
    }],
    [0x81B1, {
      name: "LIGATION JOINER",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      linebreakCategory: "'SPACE",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}JOIN`,
      unicode: [0x200D],
      ignorable: true,
    }],
    [0x81B2, {
      name: "HYPHENATION",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xAD],
      ignorable: true,
    }],
    [0x81B3, {
      name: "CHARACTER SEPARATOR",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x82],
      ignorable: true,
    }],
    [0x81B4, {
      name: "CHARACTER JOINER",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x83],
      ignorable: true,
    }],
    [0x81B5, {
      name: "MULTIGRAPH SEPARATOR",
      basicType: `${kixtNamespace}NONSPACING`,
      ...defaultAdditionalProperties,
      unicode: [0x034F],
    }],
    [0x81B6, {
      name: "QUOTE",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x99],
      ignorable: true,
    }],
    [0x81B7, {
      name: "SHIFT STRING",
      basicType: `${kixtNamespace}DATA`,
      ...defaultAdditionalProperties,
      unicode: [0x8F],
    }],
    [0x81B8, {
      name: "REFERENCE",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x8B],
      ignorable: true,
    }],
    [0x81B9, {
      name: "FILLER",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x80],
      ignorable: true,
    }],
    [0x81BA, {
      name: "JUSTIFIABLE SPACE",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      fullwidth: undefined,
      linebreakCategory: "'SPACE",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}JOIN`,
      unicode: [0xA0],
    }],
    [0x81BB, {
      name: "NARROW SPACE",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      fullwidth: false,
      linebreakCategory: "'SPACE",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}JOIN`,
      unicode: [0x202F],
    }],
    [0x81BC, {
      name: "MEDIUM QUAD",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      fullwidth: false,
      linebreakCategory: "'SPACE",
      linebreakBefore: `${kixtNamespace}JOIN`,
      linebreakAfter: `${kixtNamespace}BREAK`,
      unicode: [0x205F],
    }],
    [0x81BD, {
      name: "TIMES",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0x2062],
      ignorable: true,
    }],
    [0x81BE, {
      name: "AS TEXT",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xFE0E],
      ignorable: true,
    }],
    [0x81BF, {
      name: "AS EMOJI",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xFE0F],
      ignorable: true,
    }],
    ...function* () {
      // For simplicity, tag names have the format
      // ‹ TAG-❲codepoint❳ ›.
      for (let charCode = 0x20; charCode < 0x7F; ++charCode) {
        yield [
          0x81A0 + charCode % 0x40 + (charCode / 0x40 >>> 0) * 0x100,
          {
            name: `TAG-00${charCode.toString(16).toUpperCase()}`,
            basicType: `${kixtNamespace}FORMAT`,
            ...defaultAdditionalProperties,
            unicode: [0xE0000 + charCode],
            ignorable: true,
          },
        ];
      }
    }(),
    [0x82DF, {
      name: "CLOSE TAG",
      basicType: `${kixtNamespace}FORMAT`,
      ...defaultAdditionalProperties,
      unicode: [0xE007F],
      ignorable: true,
    }],
  ]
) {
  Object.freeze(value.unicode);
  const character = Object.freeze(
    Object.assign(
      Object.create(null, {
        aliases: {
          enumerable: true,
          get() {
            return new Set();
          },
          set: undefined,
        },
        alsoKnownAs: {
          enumerable: true,
          get() {
            return new Set();
          },
          set: undefined,
        },
        compatibility: {
          enumerable: true,
          get() {
            return [this];
          },
          set: undefined,
        },
        decomposition: {
          enumerable: true,
          get() {
            return [this];
          },
          set: undefined,
        },
        notes: {
          enumerable: true,
          get() {
            return [];
          },
          set: undefined,
        },
        references: {
          enumerable: true,
          get() {
            return new Set();
          },
          set: undefined,
        },
        toString: {
          enumerable: true,
          value: function toString() {
            return String.fromCodePoint(...this.unicode);
          },
        },
        valueOf: {
          enumerable: true,
          value: function valueOf() {
            return Math.min(this.codepoint >>> 0, 0xFFFF);
          },
        },
      }),
      value,
      {
        codepoint: +codepoint,
        compatibilityMode: `${kixtNamespace}GENERIC`,
        glyphs: Object.freeze(Object.create(null)),
      },
    ),
  );
  Object.defineProperties(requiredCharacters, {
    [codepoint]: {
      configurable: false,
      enumerable: true,
      value: character,
      writable: false,
    },
    [character.name]: {
      configurable: false,
      enumerable: false,
      value: character,
      writable: false,
    },
  });
}
export default Object.preventExtensions(requiredCharacters);
