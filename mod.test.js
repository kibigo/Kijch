// 🔤🟠 Kijch ∷ mod.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { assert } from "./dev-deps.js";
import * as Kijch from "./mod.js";

Deno.test("It exports something", () => {
  assert(Object.keys(Kijch).length > 0);
});
